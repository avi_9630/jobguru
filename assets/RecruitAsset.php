<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RecruitAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'assets/img/favicon/favicon-32x32.png',
        'assets/img/favicon/favicon-32x32.html',
        // 'assets/css/bootstrap.css',
        'assets/css/font-awesome.min.css',
        'assets/css/magnific-popup.css',
        'assets/css/owl.carousel.min.css',
        'assets/css/owl.theme.default.min.css',
        'assets/css/animate.min.css',
        'assets/css/select2.min.css',
        'assets/css/slicknav.min.css',
        'assets/css/bootstrap-datepicker.min.css',
        'assets/css/jquery-ui.min.css',
        'assets/css/perfect-scrollbar.min.css',
        'assets/css/style.css',
        'assets/css/responsive.css',
        
    ];
    public $js = [

        'assets/js/popper.min.js',
        'assets/js/bootstrap.min.js',
        'assets/js/bootstrap-datepicker.min.js',
        'assets/js/jquery-perfect-scrollbar.min.js',
        'assets/js/owl.carousel.min.js',
        'assets/js/jquery.slicknav.min.js',
        'assets/js/jquery.magnific-popup.min.js',
        'assets/js/select2.min.js',
        'assets/js/jquery-ui.min.js',
        'assets/js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
