<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        // 'assets/img/favicon/favicon-32x32.png',
        'assets/img/favicon/favicon-32x32.html',
        // 'assets/css/bootstrap.css',
        'assets/css/font-awesome.min.css',
        'assets/css/magnific-popup.css',
        'assets/css/owl.carousel.min.css',
        'assets/css/owl.theme.default.min.css',
        'assets/css/animate.min.css',
        'assets/css/select2.min.css',
        'assets/css/slicknav.min.css',
        'assets/css/bootstrap-datepicker.min.css',
        'assets/css/jquery-ui.min.css',
        'assets/css/perfect-scrollbar.min.css',
        'assets/css/style.css',
        'assets/css/responsive.css',
        'css/slider.css',
        // 'https://static.naukimg.com/s/4/101/c/common_v81.min.css',
        // 'https://static.naukimg.com/s/4/101/c/common_v81.min.css',

        // this is for theme  integration.
        // 'dashboard/bower_components/bootstrap/dist/css/bootstrap.min.css',
        // 'dashboard/bower_components/font-awesome/css/font-awesome.min.css',
        // 'dashboard/bower_components/Ionicons/css/ionicons.min.css',
        // 'dashboard/dist/css/AdminLTE.min.css',
        // 'dashboard/dist/css/skins/_all-skins.min.css',
        // 'dashboard/bower_components/morris.js/morris.css',
        // 'dashboard/bower_components/jvectormap/jquery-jvectormap.css',
        // 'dashboard/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        // 'dashboard/bower_components/bootstrap-daterangepicker/daterangepicker.css',
        // 'dashboard/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        // 'dashboard/https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',

    ];
    public $js = [
        // 'assets/js/jquery-3.0.0.min.js',
        'assets/js/popper.min.js',
        'assets/js/bootstrap.min.js',
        'assets/js/bootstrap-datepicker.min.js',
        'assets/js/jquery-perfect-scrollbar.min.js',
        'assets/js/owl.carousel.min.js',
        'assets/js/jquery.slicknav.min.js',
        'assets/js/jquery.magnific-popup.min.js',
        'assets/js/select2.min.js',
        'assets/js/jquery-ui.min.js',
        'assets/js/main.js',
        'js/slider.js',
        'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js',

        //  this js file for theme integration.......
        // 'dashboard/bower_components/jquery/dist/jquery.min.js',
        // 'dashboard/bower_components/jquery-ui/jquery-ui.min.js',
        // 'dashboard/bower_components/bootstrap/dist/js/bootstrap.min.js',
        // 'dashboard/bower_components/raphael/raphael.min.js',
        // 'dashboard/bower_components/morris.js/morris.min.js',
        // 'dashboard/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js',
        // 'dashboard/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        // 'dashboard/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        // 'dashboard/bower_components/jquery-knob/dist/jquery.knob.min.js',
        // 'dashboard/bower_components/moment/min/moment.min.js',
        // 'dashboard/bower_components/bootstrap-daterangepicker/daterangepicker.js',
        // 'dashboard/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        // 'dashboard/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        // 'dashboard/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
        // 'dashboard/bower_components/fastclick/lib/fastclick.js',
        // 'dashboard/dist/js/adminlte.min.js',
        // 'dashboard/dist/js/pages/dashboard.js',
        // 'dashboard/dist/js/demo.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
