<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DesiredCareerProfile;

/**
 * DesiredCareerProfileSearch represents the model behind the search form of `app\models\DesiredCareerProfile`.
 */
class DesiredCareerProfileSearch extends DesiredCareerProfile
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['desired_career_profile_id', 'industry_id', 'department_id', 'role_id', 'desired_location', 'desired_industry'], 'integer'],
            [['job_type', 'employement_type', 'preferred_shift', 'created_at', 'updated_at'], 'safe'],
            [['expected_salary'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DesiredCareerProfile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'desired_career_profile_id' => $this->desired_career_profile_id,
            'industry_id' => $this->industry_id,
            'department_id' => $this->department_id,
            'role_id' => $this->role_id,
            'expected_salary' => $this->expected_salary,
            'desired_location' => $this->desired_location,
            'desired_industry' => $this->desired_industry,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'job_type', $this->job_type])
            ->andFilterWhere(['like', 'employement_type', $this->employement_type])
            ->andFilterWhere(['like', 'preferred_shift', $this->preferred_shift]);

        return $dataProvider;
    }
}
