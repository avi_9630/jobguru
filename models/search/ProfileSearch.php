<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Profile;

/**
 * ProfileSearch represents the model behind the search form of `app\models\Profile`.
 */
class ProfileSearch extends Profile
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['profile_id', 'employement_detail_id', 'education_id', 'project_id', 'online_profile_id', 'desired_career_profile_id', 'personal_detail'], 'integer'],
            [['attach_resume', 'resume_headline', 'key_id', 'profile_summary', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Profile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'profile_id' => $this->profile_id,
            'employement_detail_id' => $this->employement_detail_id,
            'education_id' => $this->education_id,
            'project_id' => $this->project_id,
            'online_profile_id' => $this->online_profile_id,
            'desired_career_profile_id' => $this->desired_career_profile_id,
            'personal_detail' => $this->personal_detail,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'attach_resume', $this->attach_resume])
            ->andFilterWhere(['like', 'resume_headline', $this->resume_headline])
            ->andFilterWhere(['like', 'key_id', $this->key_id])
            ->andFilterWhere(['like', 'profile_summary', $this->profile_summary]);

        return $dataProvider;
    }
}
