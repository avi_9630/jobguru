<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PersonalDetail;

/**
 * PersonalDetailSearch represents the model behind the search form of `app\models\PersonalDetail`.
 */
class PersonalDetailSearch extends PersonalDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['personal_detail_id', 'work_permit_other_country'], 'integer'],
            [['dob', 'permanent_address', 'gender', 'hometown', 'pincode', 'marital_status', 'differently_abled', 'work_permit', 'language', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PersonalDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'personal_detail_id' => $this->personal_detail_id,
            'dob' => $this->dob,
            'work_permit_other_country' => $this->work_permit_other_country,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'permanent_address', $this->permanent_address])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'hometown', $this->hometown])
            ->andFilterWhere(['like', 'pincode', $this->pincode])
            ->andFilterWhere(['like', 'marital_status', $this->marital_status])
            ->andFilterWhere(['like', 'differently_abled', $this->differently_abled])
            ->andFilterWhere(['like', 'work_permit', $this->work_permit])
            ->andFilterWhere(['like', 'language', $this->language]);

        return $dataProvider;
    }
}
