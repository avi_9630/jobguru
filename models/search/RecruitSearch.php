<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Recruit;

/**
 * RecruitSearch represents the model behind the search form of `app\models\Recruit`.
 */
class RecruitSearch extends Recruit
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['recruit_id', 'industry_id', 'pincode_id', 'isd', 'std', 'std_number', 'country_code', 'mobile_number'], 'integer'],
            [['email', 'password', 'company_name', 'type', 'super_user', 'designation', 'office_address', 'alternate_email', 'terms_conditions', 'photo', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Recruit::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'recruit_id' => $this->recruit_id,
            'industry_id' => $this->industry_id,
            'pincode_id' => $this->pincode_id,
            'isd' => $this->isd,
            'std' => $this->std,
            'std_number' => $this->std_number,
            'country_code' => $this->country_code,
            'mobile_number' => $this->mobile_number,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'super_user', $this->super_user])
            ->andFilterWhere(['like', 'designation', $this->designation])
            ->andFilterWhere(['like', 'office_address', $this->office_address])
            ->andFilterWhere(['like', 'alternate_email', $this->alternate_email])
            ->andFilterWhere(['like', 'terms_conditions', $this->terms_conditions])
            ->andFilterWhere(['like', 'photo', $this->photo]);

        return $dataProvider;
    }
}
