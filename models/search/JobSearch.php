<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Job;

/**
 * JobSearch represents the model behind the search form of `app\models\Job`.
 */
class JobSearch extends Job
{
    /**
     * {@inheritdoc}
     */
    public $globalSearch;
    public function rules()
    {
        return [
            [['job_id'], 'integer'],
            [[  'title_name','globalSearch','user_id','skill_id','country_id','state_id','city_id', 'company_id', 'profile_id',
                'designation_id', 'discription', 'experience', 'salary', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Job::find()
        ->leftJoin("user","user.user_id = job.user_id") 
        ->leftJoin("skill","skill.skill_id = job.skill_id")
        ->leftJoin("company","company.company_id = job.company_id")
        ->leftJoin("designation","designation.designation_id = job.designation_id")
        ->leftJoin("country","country.country_id = job.country_id")
        ->leftJoin("state","state.state_id = job.state_id")
        ->leftJoin("city","city.city_id = job.city_id");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->orFilterWhere([
            'job_id' => $this->job_id,
            // 'user_id' => $this->user_id,
            // 'profile_id' => $this->profile_id,
            // 'skill_id' => $this->skill_id,
            // 'company_id' => $this->company_id,
            // 'designation_id' => $this->designation_id,
            // 'created_at' => $this->created_at,
            // 'updated_at' => $this->updated_at,
        ]);

        // $query->andFilterWhere(['like', 'title_name', $this->title_name])
        //     ->andFilterWhere(['like', 'discription', $this->discription])
        //     ->andFilterWhere(['like', 'experience', $this->experience])
        //     ->andFilterWhere(['like', 'salary', $this->salary]);


        $query->
                orFilterWhere(['like', 'title_name', $this->globalSearch])
        ->orFilterWhere(['like', 'discription', $this->globalSearch])
        ->orFilterWhere(['like', 'user.name', $this->globalSearch])
        ->orFilterWhere(['like', 'skill.name', $this->globalSearch])
        ->orFilterWhere(['like', 'company.name', $this->globalSearch])
        ->orFilterWhere(['like', 'designation.name', $this->globalSearch])
        ->orFilterWhere(['like', 'country.name', $this->globalSearch])
        ->orFilterWhere(['like', 'state.name', $this->globalSearch])
        ->orFilterWhere(['like', 'city.name', $this->globalSearch])
        // ->orFilterWhere(['like', 'experience', $this->globalSearch])
        ->orFilterWhere(['like', 'salary', $this->globalSearch]);

        return $dataProvider;
    }
}
