<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Company;

/**
 * CompanySearch represents the model behind the search form of `app\models\Company`.
 */
class CompanySearch extends Company
{
    // public $globalSearch;
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['name', 'about_company', 'rating','user_id','review', 'created_at', 'updated_at'], 'safe'],
            //globalSearch
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find();//->leftJoin("user","user.user_id = company.user_id");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            // 'company_id' => $this->company_id,
            'name' => $this->name,
            // 'user_id' => $this->user_id,
            // 'created_at' => $this->created_at,
            // 'updated_at' => $this->updated_at,
        ]);

        // $query->orFilterWhere(['like', 'name', $this->globalSearch])
        //     ->orFilterWhere(['like', 'about_company', $this->globalSearch])
        //     ->orFilterWhere(['like', 'rating', $this->globalSearch])
        //     // ->orFilterWhere(['like', 'user.name', $this->globalSearch])
        //     ->orFilterWhere(['like', 'review', $this->globalSearch]);

        return $dataProvider;
    }
}
