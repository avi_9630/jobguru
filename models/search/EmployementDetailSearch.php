<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmployementDetail;

/**
 * EmployementDetailSearch represents the model behind the search form of `app\models\EmployementDetail`.
 */
class EmployementDetailSearch extends EmployementDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employment_detail_id', 'work_till', 'state_id', 'skill_id', 'industry_id', 'functional_area_id', 'role_id', 'status'], 'integer'],
            [['current_designation', 'current_company', 'present_company', 'working_from', 'present', 'notice_period', 'job_discription', 'created_at', 'updated_at'], 'safe'],
            [['annual_salary'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmployementDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'employment_detail_id' => $this->employment_detail_id,
            'annual_salary' => $this->annual_salary,
            'working_from' => $this->working_from,
            'present' => $this->present,
            'work_till' => $this->work_till,
            'state_id' => $this->state_id,
            'skill_id' => $this->skill_id,
            'industry_id' => $this->industry_id,
            'functional_area_id' => $this->functional_area_id,
            'role_id' => $this->role_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'current_designation', $this->current_designation])
            ->andFilterWhere(['like', 'current_company', $this->current_company])
            ->andFilterWhere(['like', 'present_company', $this->present_company])
            ->andFilterWhere(['like', 'notice_period', $this->notice_period])
            ->andFilterWhere(['like', 'job_discription', $this->job_discription]);

        return $dataProvider;
    }
}
