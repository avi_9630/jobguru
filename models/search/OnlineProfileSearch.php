<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OnlineProfile;

/**
 * OnlineProfileSearch represents the model behind the search form of `app\models\OnlineProfile`.
 */
class OnlineProfileSearch extends OnlineProfile
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['online_profile_id', 'status'], 'integer'],
            [['social_profile', 'url', 'discription', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OnlineProfile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'online_profile_id' => $this->online_profile_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'social_profile', $this->social_profile])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'discription', $this->discription]);

        return $dataProvider;
    }
}
