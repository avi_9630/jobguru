<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project;

/**
 * ProjectSearch represents the model behind the search form of `app\models\Project`.
 */
class ProjectSearch extends Project
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'team_size', 'role_id', 'skill_id'], 'integer'],
            [['project_title', 'client', 'project_status', 'working_from', 'worked_till', 'project_detail', 'project_location', 'project_site', 'employement', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Project::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'project_id' => $this->project_id,
            'working_from' => $this->working_from,
            'worked_till' => $this->worked_till,
            'team_size' => $this->team_size,
            'role_id' => $this->role_id,
            'skill_id' => $this->skill_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'project_title', $this->project_title])
            ->andFilterWhere(['like', 'client', $this->client])
            ->andFilterWhere(['like', 'project_status', $this->project_status])
            ->andFilterWhere(['like', 'project_detail', $this->project_detail])
            ->andFilterWhere(['like', 'project_location', $this->project_location])
            ->andFilterWhere(['like', 'project_site', $this->project_site])
            ->andFilterWhere(['like', 'employement', $this->employement]);

        return $dataProvider;
    }
}
