<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "course".
 *
 * @property int $course_id
 * @property int $qualification_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['qualification_id', 'name'], 'required'],
            [['qualification_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'course_id' => 'Course ID',
            'qualification_id' => 'Qualification Name',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getQualification()
    {
        return $this->hasOne(Qualification::className(), ['qualification_id' => 'qualification_id']);
    }
}
