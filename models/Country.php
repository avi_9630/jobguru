<?php

namespace app\models;

use Yii;
use app\models\State;

/**
 * This is the model class for table "country".
 *
 * @property int $country_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'country_id' => 'Country Name',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getState()
    {
        return $this->hasMany(State::className(), ['country_id' => 'state_id']);
    }
}
