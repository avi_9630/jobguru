<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $user_id id is user_id
 * @property int $level_id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $authKey
 * @property int $status
 * @property string $mobile
 * @property double $total_work_experience
 * @property int $month
 * @property string $resume
 * @property int $employement_detail_id
 * @property string $duration
 * @property string $created_at
 * @property string $updated_at
 *
 * @property UserLevel $level
 */
class User extends \yii\db\ActiveRecord     implements \yii\web\IdentityInterface
{
    public $file;


    const USER_ADMIN = 1;
    const USER_EMPLOYER = 33;
    const USER_EMPLOYEE = 35;

    // public $currentPassword;
    // public $newPassword;
    // public $newPasswordConfirm;

    public function getUserOptions( $id = null) {
        $list = array(
            self::USER_ADMIN => 'Admin',
            self::USER_EMPLOYER => 'Employer',
            self::USER_EMPLOYEE => 'Employee',
        );
        if ($id === null) {
            return $list;
        }
        if (is_numeric($id)) {
            return isset($list[$id]) ? $list[$id] : 'not defined';
        }
        return $id;
    }

    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [
            [[  'name', 'email','password','mobile','total_work_experience','month'], 'required'],

            // [['newPassword', 'currentPassword','newPasswordConfirm'],'required'],
            // [['currentPassword'], 'validateCurrentPassword'],

            // [['newPassword', 'newPasswordConfirm'],'string' , 'min' => 3 ],
            // [['newPassword', 'newPasswordConfirm'],'filter', 'filter'=>'trim'],

            // [['newPasswordConfirm'], 'compare' , 'copmpareAttribute' => 'newPassword',
            //     'message' => 'password do not match'],

            // [['newPasswordConfirm'], 'compare', 'compareAttribute' => 'newPassword', 'message' => 'password do not match'],
                

            ['email', 'email'],
            [['level_id', 'status','month','total_work_experience'], 'integer'],
            
            [['created_at','authKey','resume','username','employement_detail_id','duration','level_id','status', 'updated_at'], 'safe'],
                       
            [['name','username','password'],'string', 'max' => 150 ],

            [['resume'], 'string', 'max' => 255],
            [['file'],'file'], // this is used for resume uploading

            [['mobile'], 'string', 'max' => 10],
            [['mobile'], 'string', 'min' => 10],
            [['authKey'], 'string', 'max' => 150],

            [['authKey'], 'string', 'max' => 150],

            [['level_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserLevel::className(), 'targetAttribute' => ['level_id' => 'user_level_id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'level_id' => 'Level ID',
            'name' => 'Name',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Create Password',
            'authKey' => 'Auth Key',
            'status' => 'Status',
            'mobile' => 'Mobile',
            'total_work_experience' => 'Total Work Experience',
            'month' => 'Month',
            'file' => 'Resume',
            'employement_detail_id' => 'Employement Detail ID',
            'duration' => 'Duration',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',

            // 'password' => 'Password',
            'changepassword' => 'Change Password',
            'reenterpassword' => 'Re-enter Password',
        ];
    }
    
    public function getLevel()
    {
        return $this->hasOne(UserLevel::className(), ['user_level_id' => 'level_id']);
    }
    public function getEmployementDetail()
    {
        return $this->hasOne(UserLevel::className(), ['employement_detail_id' => 'employement_detail_id']);
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException();//I don't implement this method because I don't have any access token column in my database
    }

    public function getId()
    {
        return $this->user_id;
    }

    public function getAuthKey()
    {
        return $this->authKey; //Here I return a value of my authKey column
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public static function findByEmail($email)
    {
        return self::findOne(['email' => $email]);
    }

    public function validatePassword($password)
    {
        // return $this->password === $password;
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
        // return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

}
