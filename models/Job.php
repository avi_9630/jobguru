<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "job".
 *
 * @property int $job_id
 * @property int $user_id
 * @property int $profile_id
 * @property string $title_name
 * @property string $discription
 * @property string $experience
 * @property string $salary
 * @property string $created_at
 * @property string $updated_at
 */
class Job extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'job';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id','skill_id','country_id','city_id','state_id','company_id', 'profile_id','designation_id', 'title_name', 'discription', 'experience', 'salary'], 'required'],
            [['user_id', 'profile_id','skill_id','company_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title_name', 'experience', 'salary'], 'string', 'max' => 50],
            [['discription'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            'user_id' => 'User',
            'profile_id' => 'Profile',
            'skill_id' => 'Skill',
            'company_id' => 'Company',
            'designation_id' => 'Designation',
            'title_name' => 'Title Name',
            'discription' => 'Discription',
            'experience' => 'Experience',
            'salary' => 'Salary',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['profile_id' => 'profile_id']);
    }
    public function getSkill()
    {
        return $this->hasOne(Skill::className(), ['skill_id' => 'skill_id']);
    }
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'company_id']);
    }
    public function getDesignation()
    {
        return $this->hasOne(Designation::className(), ['designation_id' => 'designation_id']);
    }
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }
    public function getState()
    {
        return $this->hasOne(State::className(), ['state_id' => 'state_id']);
    }
}
