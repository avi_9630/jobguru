<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    // public $username;
    public $password;
    public $email;
    // public $rememberMe = true;

    private $_user = false;
    // private $_recruit = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email','password'], 'required'],
            ['email', 'email'],
           
            // rememberMe must be a boolean value
            // ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [          
            
            'username' => 'Username',
            'email' => 'Email ID/ Username',
            'password' => 'Password',
            
            
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    // public function validatePassword()
    // {
    //     if (!$this->hasErrors()) {
    //         $user = $this->getUser();
    //         if (!$user) {
    //             $this->addError('password', 'Incorrect username or password.');
    //             return ;
    //         }
    //         // $recruit = $this->getRecruit();
    //         if (!$user->validatePassword($this->password)) { // you need to implement this method for both models
    //             $this->addError('password', 'Incorrect username or password.');
    //             return ;
    //         }          
    //     }
    // }

    
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser());
        }
        return false;
    }

    // public function loginR()
    // {
    //     if ($this->validate()) {
    //         return Yii::$app->recruit->loginR($this->getRecruit());
    //     }
    //     return false;
    // }

    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }

    // public function getUser()
    // {
    //     if (!$this->_user) {
    //         $this->_user = User::findByEmail($this->email);
    //     } 

    //     else if (!$this->recruit) {
    //         $this->_user = Recruit::findByEmail($this->email);
    //     }
    //     return $this->_user;
    // }

    // public function getRecruit()
    // {
    //     if ($this->_recruit === false) {
    //         $this->_recruit = Recruit::findByEmail($this->email);
    //     }
    //     return $this->_recruit;
    // }
}
