<?php

namespace app\models;

use Yii;

use yii\web\IdentityInterface;

class MultiUser extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
   private $_user;


    public static function findIdentity($id)
    {
        // $id will be something like "admin-1" or "employee-1"
        list($type, $pk) = explode('-', $id);

        switch ($type) {
            case 'user':
                $model = User::findOne($pk);
                break;
            case 'recruit':
                $model = Employee::findOne($pk);
                break;
        }

        if (isset($model)) {
            return new static(['user' => $model]);
        }
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new \yii\base\NotSupportedException();
    }
    

     public function getId()
    {
        $prefix = $this->_user instanceof User ? 'user' : 'recruit';
        return $prefix . '-' . $this->_user->getPrimaryKey();
    }

    public function getAuthKey()
    {
        $secret = Yii::$app->request->cookieValidationKey;
        return sha1($secret . get_class($this->_user) . $this->_user->getPrimaryKey());

        // return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        $secret = Yii::$app->request->cookieValidationKey;
        $computedKey = sha1($secret . get_class($this->_user) . $this->_user->getPrimaryKey());

        return $authKey === $computedKey;
    }

    public function setUser(\yii\db\BaseActiveRecord $user)
    {
        $this->_user = $user;
    }

    public function getUsername()
    {
        return $this->_user->username;
    }

}
