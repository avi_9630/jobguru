<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personal_detail".
 *
 * @property int $personal_detail_id
 * @property string $dob
 * @property string $permanent_address
 * @property string $gender
 * @property string $hometown
 * @property string $pincode
 * @property string $marital_status
 * @property string $differently_abled
 * @property string $work_permit
 * @property int $work_permit_other_country
 * @property string $language
 * @property string $created_at
 * @property string $updated_at
 */
class PersonalDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personal_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dob', 'permanent_address', 'gender', 'hometown', 'pincode', 'marital_status', 'differently_abled', 'work_permit', 'work_permit_other_country', 'language'], 'required'],
            [['dob', 'created_at', 'updated_at'], 'safe'],
            [['work_permit_other_country'], 'integer'],
            [['permanent_address', 'pincode', 'work_permit', 'language'], 'string', 'max' => 50],
            [['gender', 'differently_abled'], 'string', 'max' => 10],
            [['hometown'], 'string', 'max' => 100],
            [['marital_status'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'personal_detail_id' => 'Personal Detail ID',
            'dob' => 'Dob',
            'permanent_address' => 'Permanent Address',
            'gender' => 'Gender',
            'hometown' => 'Hometown',
            'pincode' => 'Pincode',
            'marital_status' => 'Marital Status',
            'differently_abled' => 'Differently Abled',
            'work_permit' => 'Work Permit',
            'work_permit_other_country' => 'Work Permit Other Country',
            'language' => 'Language',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
