<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property int $profile_id
 * @property string $attach_resume
 * @property string $resume_headline
 * @property string $key_id
 * @property int $employement_detail_id
 * @property int $education_id
 * @property int $project_id
 * @property string $profile_summary
 * @property int $online_profile_id
 * @property int $desired_career_profile_id
 * @property int $personal_detail
 * @property string $created_at
 * @property string $updated_at
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['resume_headline', 'key_id', 'employement_detail_id', 'education_id', 'project_id', 'profile_summary', 'online_profile_id', 'desired_career_profile_id', 'personal_detail'], 'required'],

            [['employement_detail_id', 'education_id', 'project_id', 'online_profile_id', 'desired_career_profile_id', 'personal_detail'], 'integer'],
            
            [['created_at', 'updated_at'], 'safe'],
            [['attach_resume'], 'file'],
            [['resume_headline'], 'string', 'max' => 50],
            [['key_id', 'profile_summary'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'profile_id' => 'Profile ID',
            'attach_resume' => 'Attach Resume',
            'resume_headline' => 'Resume Headline',
            'key_id' => 'Key skills',
            'employement_detail_id' => 'Employement Detail ID',
            'education_id' => 'Education ID',
            'project_id' => 'Project ID',
            'profile_summary' => 'Profile Summary',
            'online_profile_id' => 'Online Profile ID',
            'desired_career_profile_id' => 'Desired Career Profile ID',
            'personal_detail' => 'Personal Detail',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
