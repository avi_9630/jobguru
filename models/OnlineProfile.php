<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "online_profile".
 *
 * @property int $online_profile_id
 * @property string $social_profile
 * @property string $url
 * @property string $discription
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class OnlineProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'online_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['social_profile', 'url', 'discription'], 'required'],
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['social_profile', 'url'], 'string', 'max' => 50],
            [['discription'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'online_profile_id' => 'Online Profile ID',
            'social_profile' => 'Social Profile',
            'url' => 'Url',
            'discription' => 'Discription',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
