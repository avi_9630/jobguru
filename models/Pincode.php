<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pincode".
 *
 * @property int $pincode_id
 * @property int $city_id
 * @property int $picode_number
 * @property string $created_at
 * @property string $updated_at
 */
class Pincode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pincode';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'picode_number'], 'required'],
            [['city_id', 'picode_number'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pincode_id' => 'Pincode ID',
            'city_id' => 'City ID',
            'picode_number' => 'Picode Number',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    public function getState()
    {
        return $this->hasOne(State::className(), ['state_id' => 'state_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }
}
