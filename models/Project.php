<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property int $project_id
 * @property string $project_title
 * @property string $client
 * @property string $project_status
 * @property string $working_from
 * @property string $worked_till
 * @property string $project_detail
 * @property string $project_location
 * @property string $project_site
 * @property string $employement
 * @property int $team_size
 * @property int $role_id
 * @property int $skill_id
 * @property string $created_at
 * @property string $updated_at
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_title','month','client', 'project_status', 'working_from',
            'worked_till', 'project_detail', 'project_location', 'project_site', 'employement', 'team_size', 'role_id', 'skill_id'], 'required'],
            [['working_from', 'worked_till', 'created_at', 'updated_at'], 'safe'],
            [['team_size', 'role_id'], 'integer'],
            [['project_title', 'project_detail', 'project_location'], 'string', 'max' => 100],
            [['client'], 'string', 'max' => 50],
            [['project_status', 'project_site', 'employement'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'project_id' => 'Project ID',
            'project_title' => 'Project Title',
            'client' => 'Client',
            'project_status' => 'Project Status',
            'working_from' => 'Started Working From',
            'worked_till' => 'Worked Till',
            // 'worked_till' => 'Month',
            'project_detail' => 'Project Detail',
            'project_location' => 'Project Location',
            'project_site' => 'Project Site',
            'employement' => 'Nature of Employement',
            'team_size' => 'Team Size',
            'role_id' => 'Role ID',
            'skill_id' => 'Skill ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getYearsList() {
        $currentYear = date('Y');
        $yearFrom = 2000;
        $yearsRange = range($yearFrom, $currentYear);
        return array_combine($yearsRange, $yearsRange);
    }
    // 
}
