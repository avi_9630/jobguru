<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "state".
 *
 * @property int $state_id
 * @property int $country_id
 * @property int $name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property City[] $cities
 */
class State extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'state';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'name'], 'required'],
                    [['name'],'string', 'max' => 150 ],
            [['name'], 'unique','targetAttribute'=>['name'],'message' =>'Duplicate not allowed'],
            // [['country_id', 'name'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'state_id' => 'State ID',
            'country_id' => 'Country Name',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }
    public function getCity()
    {
        return $this->hasMany(City::className(), ['state_id' => 'city_id']);
    }
    
}
