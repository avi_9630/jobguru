<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "functional_area".
 *
 * @property int $functional_area_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class FunctionalArea extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'functional_area';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'functional_area_id' => 'Functional Area ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
