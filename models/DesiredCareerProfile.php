<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "desired_career_profile".
 *
 * @property int $desired_career_profile_id
 * @property int $industry_id
 * @property int $department_id
 * @property int $role_id
 * @property string $job_type
 * @property string $employement_type
 * @property string $preferred_shift
 * @property double $expected_salary
 * @property int $desired_location
 * @property int $desired_industry
 * @property string $created_at
 * @property string $updated_at
 */
class DesiredCareerProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'desired_career_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['industry_id', 'department_id', 'role_id', 'job_type', 'employement_type', 'preferred_shift', 'expected_salary', 'desired_location', 'desired_industry'], 'required'],
            [['industry_id', 'department_id', 'role_id', 'desired_location', 'desired_industry'], 'integer'],
            [['expected_salary'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['job_type', 'employement_type', 'preferred_shift'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'desired_career_profile_id' => 'Desired Career Profile ID',
            'industry_id' => 'Industry ID',
            'department_id' => 'Department ID',
            'role_id' => 'Role ID',
            'job_type' => 'Job Type',
            'employement_type' => 'Employement Type',
            'preferred_shift' => 'Preferred Shift',
            'expected_salary' => 'Expected Salary',
            'desired_location' => 'Desired Location',
            'desired_industry' => 'Desired Industry',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
