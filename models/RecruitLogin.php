<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Recruit;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RecruitLogin extends Model
{
    // public $username;
    public $password;
    public $email;
    // public $rememberMe = true;

    private $_recruit = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email','password'], 'required'],
            ['email', 'email'],
           
            // rememberMe must be a boolean value
            // ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [          
            
            'username' => 'Username',
            'email' => 'Email ID/ Username',
            'password' => 'Password',
            
            
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $recruit = $this->getRecruit();

            if (!$recruit || !$recruit->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->recruit->login($this->getRecruit());
        }
        return false;
    }

    public function getRecruit()
    {
        if ($this->_recruit === false) {
            $this->_recruit = Recruit::findByEmail($this->email);
        }
        return $this->_recruit;
    }
}
