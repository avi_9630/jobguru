<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employement_detail".
 *
 * @property int $employment_detail_id
 * @property string $current_designation
 * @property string $current_company
 * @property string $present_company
 * @property double $annual_salary
 * @property string $working_from
 * @property string $present
 * @property int $work_till
 * @property string $notice_period
 * @property int $skill_id
 * @property int $industry_id
 * @property int $functional_area_id
 * @property int $role_id
 * @property string $job_discription
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class EmployementDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employement_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[  
                'current_designation', 'current_company', 'annual_salary','thousands', 'working_from','month',
                'present', 'country_id','city_id', 'notice_period', 'skill_id'], 'required'],
            
            [[  'working_from','month','present', 'created_at','work_till','industry_id','functional_area_id',
                'updated_at','role_id', 'job_discription'], 'safe'],

            [[  
                'work_till', 'country_id', 'industry_id',
                'functional_area_id','city_id','thousands','annual_salary',
                'role_id', 'status'], 'integer'],

            [['current_designation', 'current_company', 'job_discription'], 'string', 'max' => 100],
            [['present_company', 'notice_period'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'employment_detail_id' => 'Employment Detail ID',
            'current_designation' => 'Current Designation',
            'current_company' => 'Current Company',
            'present_company' => 'Present Company',
            'annual_salary' => 'Annual Salary',
            'thousands' => 'Thousands',
            'working_from' => 'Working Since',
            'month' => 'Month',
            'present' => 'Present',
            'work_till' => 'Work Till',
            'country_id' => 'Country',
            'city_id' => 'Current Location',
            'notice_period' => 'Duration of Notice Period',
            'skill_id' => 'Skill ID',
            'industry_id' => 'Industry ID',
            'functional_area_id' => 'Functional Area ID',
            'role_id' => 'Role ID',
            'job_discription' => 'Job Discription',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getYearsList() {
        $currentYear = date('Y');
        $yearFrom = 1980;
        $yearsRange = range($yearFrom, $currentYear);
        return array_combine($yearsRange, $yearsRange);
    }

    public function getMonthList() {
        $currentYear = date('Y');
        // $persent = "Persent";
        $yearFrom = 1980;
        $yearsRange = range($yearFrom, $currentYear);
        return array_combine($yearsRange, $yearsRange);
    }
}
