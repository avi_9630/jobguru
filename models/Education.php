<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "education".
 *
 * @property int $education_id
 * @property int $specialization_id
 * @property string $university_college
 * @property string $course_type
 * @property string $passing_year
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class Education extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'education';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['specialization_id', 'university_college', 'course_type', 'passing_year'], 'required'],
            [['specialization_id', 'status'], 'integer'],
            [['passing_year', 'created_at', 'updated_at'], 'safe'],
            [['university_college'], 'string', 'max' => 100],
            [['course_type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'education_id' => 'Education ID',
            'specialization_id' => 'Specialization ID',
            'university_college' => 'University College',
            'course_type' => 'Course Type',
            'passing_year' => 'Passing Year',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
