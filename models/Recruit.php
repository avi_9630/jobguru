<?php

namespace app\models;

use Yii;
use app\models\Recruit;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "recruit".
 *
 * @property int $recruit_id
 * @property string $email
 * @property string $password
 * @property string $company_name
 * @property int $industry_id
 * @property string $type
 * @property string $super_user
 * @property string $designation
 * @property string $office_address
 * @property int $pincode_id
 * @property int $isd
 * @property int $std
 * @property int $std_number
 * @property int $country_code
 * @property int $mobile_number
 * @property string $alternate_email
 * @property string $terms_conditions
 * @property string $photo
 * @property string $created_at
 * @property string $updated_at
 */
class Recruit extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

   public $file;


    public static function tableName()
    {
        return 'recruit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'email', 'password', 'company_name', 'industry_id','type','super_user','designation', 'office_address', 'pincode_id','isd', 'std', 'std_number','country_code', 'mobile_number',
                'alternate_email','terms_conditions'], 'required'],

            [['email','alternate_email'], 'email'],

            [['industry_id', 'pincode_id', 'isd', 'std', 'std_number', 'country_code', 'mobile_number'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['email', 'company_name', 'type', 'super_user', 'designation', 'alternate_email'], 'string', 'max' => 50],
            [['password', 'office_address'], 'string', 'max' => 100],
            [['terms_conditions'], 'string', 'max' => 5],
            [['photo'], 'string', 'max' => 300],
            [['file'], 'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'recruit_id' => 'Recruit ID',
            'email' => 'Email',
            'password' => 'Password',
            'company_name' => 'Company Name',
            'industry_id' => 'Industry ID',
            'type' => 'Type',
            'super_user' => 'Super User',
            'designation' => 'Designation',
            'office_address' => 'Office Address',
            'pincode_id' => 'Pincode ID',
            'isd' => 'Isd',
            'std' => 'Std',
            'std_number' => 'Std Number',
            'country_code' => 'Country Code',
            'mobile_number' => 'Mobile Number',
            'alternate_email' => 'Alternate Email',
            'terms_conditions' => 'Terms Conditions',
            'file' => 'Photo',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException();
    }

    public function getId()
    {
        return $this->recruit_id;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public static function findByEmail($email)
    {
        return self::findOne(['email' => $email]);  
    }

    public function validatePassword($password)
    {
        // return $this->password === $password;
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
        // return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

}
