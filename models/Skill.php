<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "skill".
 *
 * @property int $skill_id
 * @property string $name
 * @property string $version
 * @property string $last_used
 * @property string $experience
 * @property string $created_at
 * @property string $updated_at
 */
class Skill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'skill';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'version', 'last_used', 'experience'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'last_used', 'experience'], 'string', 'max' => 50],
            [['version'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'skill_id' => 'Skill ID',
            'name' => 'Name',
            'version' => 'Version',
            'last_used' => 'Last Used',
            'experience' => 'Experience',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getJob()
    {
        return $this->hasMany(Job::className(), ['skill_id' => 'job_id']);
    }
}
