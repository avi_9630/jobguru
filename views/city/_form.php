<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\State;
use app\models\Country;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        echo $form->field(new Country(), 'country_id')->dropDownList(
            ArrayHelper::map(Country::find()->all(), "country_id", "name"),
            [
                'prompt' => 'Select Country',
                'id' => 'country_id',
                'onchange' => 'getAllState()',
                    ]
                )->label("Country");
    ?>

    <?= $form->field($model, 'state_id')->dropDownList(ArrayHelper::map(State::find()->all(),'state_id','name'),['prompt'=>'Select State','id' => 'getState']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function getAllState(){
        var country_id = document.getElementById("country_id").value;
        
        $.ajax({
            url: '<?php echo Url::to(["site/lists"]); ?>',
            type: 'post',
            data: {
                type : 1,
                id : country_id
            },
            success: function (res) {
                var nandghar = JSON.parse(res);
                var areaOption = "<option value=''>Select State</option>";
                 for (var i = 0; i < nandghar.length; i++) {
                     areaOption += '<option value="' + nandghar[i]['state_id'] + '">' + nandghar[i]
                     ['name'] + '</option>'
                 }
                $("#getState").html(areaOption);

                if(document.getElementById("getState") !== null){
                    $("#getState").val($("#getState").val()).change();
                }
            },
            error: function (res) {
            }
        }); 
    }
</script>