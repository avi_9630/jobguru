<?php


use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\icons\Icon;
Icon::map($this);

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<style type="text/css">
   .jobguru-banner-area {
    
    /*width: 1317px;*/
        margin-left: calc(50% - 50vw);
        margin-right: calc(50% - 50vw);
        /*max-width: 1000%;*/
        width: auto;
        margin-top: -20px;
    }
    .wrap {
        min-height: 100%;
        height: auto;
        margin: 0 auto -60px;
        padding: 0 0 60px;
        background-color: #EEEEEE;
    }
    .banner-search h4{
        margin-right: 784px;
    }
    label{
        display: none;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
    }
    .help-block {
        display: none;
        margin-top: 5px;
        margin-bottom: 10px;
        color: #737373;
    }
    .banner-single-slider {
        background-color: #eee;
        background-position: center center !important;
        background-size: cover !important;
        background-repeat: no-repeat !important;
        height: 300px;
    }
    .section_70 {
      padding: 24px 0;
   }
   .section_70 {
    padding: 24px 0;
    margin-right: -30px;
   }
</style>
<style type="text/css">
    .card-title {
      padding: 32px 20px 10px;
      margin-bottom: 0;
      border-bottom: solid 1px #eee;
   }
    .svg-inline--fa {
     
      color: #1E8D4E;
     
    }
</style>
<style type="text/css">
  .jobguru-inner-hire-area {
    background:  <?php echo Url::to('@web/images/hire.jpg'); ?> !important;
    position: relative;
    z-index: 1;
  } 
</style> -->
<style type="text/css">
  .wrap {
    background-color: #ffffff;
  }
</style>
<style type="text/css">
  .how-works-box {
    text-align: center;
    position: relative;
    border-radius: 50%;
    height: 250px;
    padding: 75px 30px;
    margin: 70px auto 0;
    width: 250px;
    z-index: 1;
    background: #07474e none repeat scroll 0 0;
  }
</style>

<!--  -->
<section class="jobguru-banner-area" >
   <div class="banner-slider owl-carousel owl-loaded owl-drag" >      
      <div class="owl-stage-outer">
         <div class="owl-stage" style="transform: translate3d(-2698px, 0px, 0px); transition: all 0s ease 0s; width: 8094px;">
            <div class="owl-item cloned" style="width: 1349px;">
               <div class="banner-single-slider slider-item-1">
                  <div class="slider-offset"></div>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 1349px;">
               <div class="banner-single-slider slider-item-2">
               <div class="slider-offset"></div>
               </div>
            </div>
            <div class="owl-item active" style="width: 1349px;">
               <div class="banner-single-slider slider-item-1">
                  <div class="slider-offset"></div>
               </div>
            </div>
            <div class="owl-item" style="width: 1349px;">
               <div class="banner-single-slider slider-item-2">
               <div class="slider-offset"></div>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 1349px;">
               <div class="banner-single-slider slider-item-1">
               <div class="slider-offset"></div>
               </div>
            </div>
            <div class="owl-item cloned" style="width: 1349px;">
               <div class="banner-single-slider slider-item-2">
               <div class="slider-offset"></div>
               </div>
            </div>
         </div>
      </div>   
   </div>
   <div class="banner-text">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <form>
                  <div class="banner-form-box">
                     <div class="banner-form-input">
                        <input type="text" placeholder="Job Title, Keywords, or Phrase">
                     </div>
                     
                     <div class="banner-form-input">
                        <button type="submit">Search</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!--  -->

<section class="jobguru-categories-area section_70">
   <div class="container" >
      <div class="row" >
         <div class="col-md-12" style="background-color: white">         
            <section class="customer-logos slider">
               <div class="slide"><img src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg"></div>
               <div class="slide"><img src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg"></div>
               <div class="slide"><img src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg"></div>
               <div class="slide"><img src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg"></div>
               <div class="slide"><img src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg"></div>
               <div class="slide"><img src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg"></div>
               <div class="slide"><img src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg"></div>
               <div class="slide"><img src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg"></div>
               <div class="slide"><img src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg"></div>
               <div class="slide"><img src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg"></div>
            </section>
         </div>
      </div>      
   </div>
</section>
       
<!-- Categories Area Start -->
<section class="jobguru-categories-area section_70" style="background-color: white">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="site-heading">
               <h2>top Trending <span>Categories</span></h2>
               <p style="color: black">A better career is out there. We'll help you find it. We're your first step to becoming everything you want to be.</p>
            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="#" class="single-category-holder account_cat">
               <div class="category-holder-icon">
                <!-- <i class="fa fa-briefcase"></i> -->
                  <?php echo Icon::show('fa fa-briefcase'); ?>
               </div>
               <div class="category-holder-text">
                  <h3>Accounting &amp; Finance</h3>
               </div>
               <img src="<?php echo Url::to   ("@web/images/account.jpg"); ?>" alt="category">
            </a>
         </div>

         <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="#" class="single-category-holder account_cat">
               <div class="category-holder-icon">
                  <?php echo Icon::show('fa fa-pencil-square-o'); ?>
               </div>
               <div class="category-holder-text">
                  <h3>Design, Art &amp; Multimedia</h3>
               </div>
               <img src="<?php echo Url::to   ("@web/images/design.jpg"); ?>" alt="category">
            </a>
         </div>

         <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="#" class="single-category-holder account_cat">
               <div class="category-holder-icon">
                  <?php echo Icon::show('fa fa-cutlery'); ?>
               </div>
               <div class="category-holder-text">
                  <h3>Restaurant / Food Service</h3>
               </div>
               <img src="<?php echo Url::to   ("@web/images/food.jpg"); ?>" alt="category">
            </a>
         </div>

         <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="#" class="single-category-holder account_cat">
               <div class="category-holder-icon">
                  <?php echo Icon::show('fa fa-code'); ?>
               </div>
               <div class="category-holder-text">
                  <h3>Programming &amp; Tech</h3>
               </div>
               <img src="<?php echo Url::to   ("@web/images/code.jpg"); ?>" alt="category">
            </a>
         </div>

      </div>

      <div class="row">
         <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="#" class="single-category-holder account_cat">
               <div class="category-holder-icon">
                  <?php echo Icon::show('fa fa-bar-chart'); ?>
               </div>
               <div class="category-holder-text">
                  <h3>Data Science &amp; Analitycs</h3>
               </div>
               <img src="<?php echo Url::to   ("@web/images/science.jpg"); ?>" alt="category">
            </a>
         </div>
         <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="#" class="single-category-holder account_cat">
               <div class="category-holder-icon">                 
                  <?php echo Icon::show('fa fa-pencil'); ?>
               </div>
               <div class="category-holder-text">
                  <h3>Writing / Translations</h3>
               </div>
               <img src="<?php echo Url::to   ("@web/images/translation.jpg"); ?>" alt="category">
            </a>
         </div>
         <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="#" class="single-category-holder account_cat">
               <div class="category-holder-icon">
                   <?php echo Icon::show('fa fa-graduation-cap'); ?>
               </div>
               <div class="category-holder-text">
                  <h3>Education / Training</h3>
               </div>
               <img src="<?php echo Url::to   ("@web/images/education.jpg"); ?>" alt="category">
            </a>
         </div>
         <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="#" class="single-category-holder account_cat">
               <div class="category-holder-icon">
                  <?php echo Icon::show('fa fa-bullhorn'); ?>
               </div>
               <div class="category-holder-text">
                  <h3>sales / marketing</h3>
               </div>
               <img src="<?php echo Url::to   ("@web/images/sales.jpg"); ?>" alt="category">
            </a>
         </div> 
      </div>
   </div>
      <div class="row">
         <div class="col-md-12">
            <div class="load-more">
               <a href="#" class="jobguru-btn">browse all categories</a>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Categories Area End -->
 
<!-- Inner Hire Area Start -->
<section class="jobguru-inner-hire-area section_100">
         <div class="hire_circle"></div>
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="inner-hire-left">
                     <h3>Hire an employee</h3>
                     <p>placerat congue dui rhoncus sem et blandit .et consectetur Fusce nec nunc lobortis lorem ultrices facilisis. Ut dapibus placerat blandit nunc.congue dui rhoncus sem et blandit .et consectetur Fusce nec nunc lobortis lorem ultrices facilisis. Ut dapibus placerat blandi </p>
                     <a href="#" class="jobguru-btn-3">sign up as company</a>
                  </div>
               </div>
            </div>
         </div>
</section>
<!-- Inner Hire Area End -->
 
<section class="jobguru-job-tab-area section_70" style="background-color: white">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="site-heading">
               <h2>Companies &amp; <span>job offers</span></h2>
               <p>It's easy. Simply post a job you need completed and receive competitive bids from freelancers within minutes</p>
            </div>
         </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class=" job-tab">
            <ul class="nav nav-pills job-tab-switch" id="pills-tab" role="tablist">
                <li class="nav-item">
                   <a class="nav-link active show" id="pills-companies-tab" data-toggle="pill" href="#pills-companies" role="tab" aria-controls="pills-companies" aria-selected="true">top Companies</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-job-tab" data-toggle="pill" href="#pills-job" role="tab" aria-controls="pills-job" aria-selected="false">job openning</a>
                </li>
            </ul>
          </div>
          <div class="tab-content" id="pills-tabContent">
             <div class="tab-pane fade active show" id="pills-companies" role="tabpanel"
                  aria-labelledby="pills-companies-tab">
                <div class="top-company-tab">
                   <ul>
                      <li>
                         <div class="top-company-list">
                            <div class="company-list-logo">
                               <a href="#">
                               <img src="<?php echo Url::to('@web/images/logo.png'); ?>" alt="company list 1">
                               </a>
                            </div>
                            <div class="company-list-details">
                               <h3><a target="_blank" href="http://themescare.com/demos/jobguru/index.html">jamulai - consulting &amp; finance Co.</a></h3>
                               <p class="company-state"><i class="fa fa-map-marker"></i> Chicago, Michigan</p>
                               <p class="open-icon"><i class="fa fa-briefcase"></i>32 open position</p>
                               <p class="varify"><i class="fa fa-check"></i>Verified</p>
                               <p class="rating-company">4.9</p>
                            </div>
                            <div class="company-list-btn">
                               <a href="#" class="jobguru-btn">view profile</a>
                            </div>
                         </div>
                      </li>
                      <li>
                         <div class="top-company-list">
                            <div class="company-list-logo">
                               <a href="#">
                               <img src="<?php echo Url::to('@web/images/logo.png'); ?>" alt="company list 1">
                               </a>
                            </div>
                            <div class="company-list-details">
                               <h3><a target="_blank" href="http://themescare.com/demos/jobguru/index.html">Buildo - construction Co.</a></h3>
                               <p class="company-state"><i class="fa fa-map-marker"></i> Chicago, Michigan</p>
                               <p class="open-icon"><i class="fa fa-briefcase"></i>32 open position</p>
                               <p class="varify"><i class="fa fa-check"></i>Verified</p>
                               <p class="rating-company">4.2</p>
                            </div>
                            <div class="company-list-btn">
                               <a href="#" class="jobguru-btn">view profile</a>
                            </div>
                         </div>
                      </li>
                      <li>
                         <div class="top-company-list">
                            <div class="company-list-logo">
                               <a href="#">
                               <img src="<?php echo Url::to('@web/images/logo.png'); ?>" alt="company list 1">
                               </a>
                            </div>
                            <div class="company-list-details">
                               <h3><a target="_blank" href="http://themescare.com/demos/jobguru/index.html">palms - school &amp; college.</a></h3>
                               <p class="company-state"><i class="fa fa-map-marker"></i> Chicago, Michigan</p>
                               <p class="open-icon"><i class="fa fa-briefcase"></i>32 open position</p>
                               <p class="varify"><i class="fa fa-check"></i>Verified</p>
                               <p class="rating-company">4.6</p>
                            </div>
                            <div class="company-list-btn">
                               <a href="#" class="jobguru-btn">view profile</a>
                            </div>
                         </div>
                      </li>
                      <li>
                         <div class="top-company-list">
                            <div class="company-list-logo">
                               <a href="#">
                               <img src="<?php echo Url::to('@web/images/logo.png'); ?>" alt="company list 1">
                               </a>
                            </div>
                            <div class="company-list-details">
                               <h3><a target="_blank" href="http://themescare.com/demos/jobguru/index.html">finance - consulting &amp; business Co.</a></h3>
                               <p class="company-state"><i class="fa fa-map-marker"></i> Chicago, Michigan</p>
                               <p class="open-icon"><i class="fa fa-briefcase"></i>32 open position</p>
                               <p class="varify"><i class="fa fa-check"></i>Verified</p>
                               <p class="rating-company">4.9</p>
                            </div>
                            <div class="company-list-btn">
                               <a href="#" class="jobguru-btn">view profile</a>
                            </div>
                         </div>
                      </li>
                   </ul>
                </div>
             </div>
             <div class="tab-pane fade" id="pills-job" role="tabpanel" aria-labelledby="pills-job-tab">
                <div class="top-company-tab">
                   <ul>
                      <li>
                         <div class="top-company-list">
                            <div class="company-list-logo">
                               <a href="#">
                               <img src="<?php echo Url::to('@web/images/logo.png'); ?>" alt="company list 1">
                               </a>
                            </div>
                            <div class="company-list-details">
                               <h3><a href="#">Regional Sales Manager</a></h3>
                               <p class="company-state"><i class="fa fa-map-marker"></i> Chicago, Michigan</p>
                               <p class="open-icon"><i class="fa fa-clock-o"></i>2 minutes ago</p>
                               <p class="varify"><i class="fa fa-check"></i>Fixed price : $1200-$2000</p>
                               <p class="rating-company">4.1</p>
                            </div>
                            <div class="company-list-btn">
                               <a href="#" class="jobguru-btn">Apply Now</a>
                            </div>
                         </div>
                      </li>
                      <li>
                         <div class="top-company-list">
                            <div class="company-list-logo">
                               <a href="#">
                               <img src="<?php echo Url::to('@web/images/logo.png'); ?>" alt="company list 1">
                               </a>
                            </div>
                            <div class="company-list-details">
                               <h3><a href="#">C Developer (Senior) C .Net</a></h3>
                               <p class="company-state"><i class="fa fa-map-marker"></i> Chicago, Michigan</p>
                               <p class="open-icon"><i class="fa fa-clock-o"></i>2 minutes ago</p>
                               <p class="varify"><i class="fa fa-check"></i>Fixed price : $800-$1200</p>
                               <p class="rating-company">3.1</p>
                            </div>
                            <div class="company-list-btn">
                               <a href="#" class="jobguru-btn">Apply Now</a>
                            </div>
                         </div>
                      </li>
                      <li>
                         <div class="top-company-list">
                            <div class="company-list-logo">
                               <a href="#">
                               <img src="<?php echo Url::to('@web/images/logo.png'); ?>" alt="company list 1">
                               </a>
                            </div>
                            <div class="company-list-details">
                               <h3><a href="#">Asst. Teacher</a></h3>
                               <p class="company-state"><i class="fa fa-map-marker"></i> Chicago, Michigan</p>
                               <p class="open-icon"><i class="fa fa-clock-o"></i>3 minutes ago</p>
                               <p class="varify"><i class="fa fa-check"></i>Fixed price : $800-$1200</p>
                               <p class="rating-company">4.3</p>
                            </div>
                            <div class="company-list-btn">
                               <a href="#" class="jobguru-btn">Apply Now</a>
                            </div>
                         </div>
                      </li>
                      <li>
                         <div class="top-company-list">
                            <div class="company-list-logo">
                               <a href="#">
                               <img src="<?php echo Url::to('@web/images/logo.png'); ?>" alt="company list 1">
                               </a>
                            </div>
                            <div class="company-list-details">
                               <h3><a href="#">civil engineer</a></h3>
                               <p class="company-state"><i class="fa fa-map-marker"></i> Chicago, Michigan</p>
                               <p class="open-icon"><i class="fa fa-clock-o"></i>30 minutes ago</p>
                               <p class="varify"><i class="fa fa-check"></i>Fixed price : $2000-$2500</p>
                               <p class="rating-company">3.7</p>
                            </div>
                            <div class="company-list-btn">
                               <a href="#" class="jobguru-btn">Apply Now</a>
                            </div>
                         </div>
                      </li>
                   </ul>
                </div>
             </div>
          </div>
        </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="load-more">
               <a href="#" class="jobguru-btn    ">browse more listing</a>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Job Tab Area End -->
 
 <style type="text/css">
   .popup-youtube i {
      background: #fff none repeat scroll 0 0;
      border: 2px solid #fff;
      border-radius: 50%;
      color: #25AD60;
      display: inline-block;
      font-size: 20px;
      height: 50px;
      line-height: 8px;
      margin: 0 10px 0 0;
      padding: 20px 20px 20px 18px;
      text-align: center;
      width: 50px;
      -webkit-transition: all 0.4s ease 0s;
      transition: all 0.4s ease 0s;
    }
    .fa {
      display: inline-block;
      font: normal normal normal 14px/1 FontAwesome;
      font-size: inherit;
      text-rendering: auto;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }

 </style>

<!-- Video Area Start -->
<section class="jobguru-video-area section_100">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="video-container">
               <h2>Hire experts freelancers today for <br> any job, any time.</h2>
               <div class="video-btn">
                  <a class="popup-youtube" href="<?php echo Url::to('@web/assets/img/video_bg1.mp4') ?>">
                  <i class="fa fa-play"></i>
                  how it works
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Video Area End -->

<!-- How Works Area Start -->
<section class="how-works-area section_70">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="site-heading">
               <h2>how it <span>works</span></h2>
               <p>It's easy. Simply post a job you need completed and receive competitive bids from freelancers within minutes</p>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-4">
            <div class="how-works-box box-1">
               <!-- <img src="<?php echo Url::to('@web/images/arrowright.png'); ?>" alt="works">  -->
               <div class="works-box-icon">
                  <!-- <i class="fa fa-user"></i> -->
               </div>
               <div class="works-box-text">
                  <p style="font-size: 30px; color: white; margin-top: 60px;">sign up</p>
               </div>
            </div>
         </div>
         
         <div class="col-md-4">
            <div class="how-works-box box-2">
               <!-- <img src="assets/img/arrow-right-bottom.png" alt="works"> -->
               <div class="works-box-icon">
                  <!-- <i class="fa fa-gavel"></i> -->
               </div>
               <div class="works-box-text">
                  <p style="font-size: 30px; color: white; margin-top: 60px;">post job</p>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="how-works-box box-3">
               <div class="works-box-icon">
                  <!-- <i class="fa fa-thumbs-up"></i> -->
               </div>
               <div class="works-box-text">
                  <p style="font-size: 30px; color: white; margin-top: 60px;">choose expert</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- How Works Area End -->

<!-- Blog Area Start -->
<section class="jobguru-blog-area section_70">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="site-heading">
               <h2>Recent From <span>Blog</span></h2>
               <p style="color: black">It's easy. Simply post a job you need completed and receive competitive bids from freelancers within minutes</p>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-4 col-md-12">
            <a href="#">
               <div class="single-blog">
                  <div class="blog-image">
                     <img src="<?php echo Url::to('@web/images/education.jpg') ?>" alt="blog image">
                     <p><span> 21</span> July</p>
                  </div>
                  <div class="blog-text">
                     <h3>If you're having trouble coming up with</h3>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-lg-4 col-md-12">
            <a href="#">
               <div class="single-blog">
                  <div class="blog-image">
                     <img src="<?php echo Url::to('@web/images/education.jpg') ?>" alt="blog image">
                     <p><span> 21</span> July</p>
                  </div>
                  <div class="blog-text">
                     <h3>details about Apple’s new iPad Pro models</h3>
                  </div>
               </div>
            </a>
         </div>
         <div class="col-lg-4 col-md-12">
            <a href="#">
               <div class="single-blog">
                  <div class="blog-image">
                     <img src="<?php echo Url::to('@web/images/education.jpg') ?>" alt="blog image">
                     <p><span> 21</span> July</p>
                  </div>
                  <div class="blog-text">
                     <h3>what are those Steps to be a Successful developer</h3>
                  </div>
               </div>
            </a>
         </div>
      </div>
   </div>
</section>
<!-- Blog Area End -->

<div class="btntoTop"></div>