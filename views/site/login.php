<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';

?>

<style type="text/css">
    .form-horizontal .control-label {
        display: none;
    }
    .form-control {
        margin-left: 80px;
        width: 150%;
        height: 41px; 
    }

    .btn {
        height: 50px;
        width: 400px;
    }
    .has-error .help-block, .has-error .control-label, .has-error .radio, .has-error .checkbox, .has-error .radio-inline, .has-error .checkbox-inline, .has-error.radio label, .has-error.checkbox label, .has-error.radio-inline label, .has-error.checkbox-inline label {
        color: #a94442;
        margin-left: 80px;
    }
    
    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 100;
        font-size: small;
    }
    .has-error .help-block, .has-error .control-label, .has-error .radio, .has-error .checkbox, .has-error .radio-inline, .has-error .checkbox-inline, .has-error.radio label, .has-error.checkbox label, .has-error.radio-inline label, .has-error.checkbox-inline label {
        color: #cc2623;
        margin-left: 85px;
        width: 300px;
        font-size: 13px;
    }
    a{
      color: #79B9CF;
    }

</style>

<div class="site-login">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body" style="text-align:left;"> 
        <h4 style="margin-left: 35px; "><b>Login</b></h4><br>
          <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'layout' => 'horizontal',
            'enableAjaxValidation' => false
          ]); ?>
        
          <label style="margin-left: 80px">Email ID/Username<span style="color: red">*</span></label>

          <?= $form->field($model, 'email')->textInput(array('placeholder' => 'Username or email')); ?>
        
          <label style="margin-left: 80px">Password<span style="color: red">*</span></label>

          <?= $form->field($model, 'password')->passwordInput(array('placeholder' => 'Password'))->label('Password<span>*</span>'); ?>
                
          <p style="margin-left: 80px; ">
            <?= Html::a('Forget password?', ['site/request-password-reset'],['target'=>'_blank']) ?>
          </p> <br><br>          

          <div class="form-group" style="margin-left: 80px">
              <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
          </div>
 
          <p class="register" style="margin-left: 150px;">
              Not a member as yet?<?= Html::a('Register Now', ['site/register'],['target'=>'_blank']) ?>
          </p>
 
        <?php ActiveForm::end(); ?>
 
      </div>
    </div>
  </div>
</div>