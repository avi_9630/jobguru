<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
// use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\UserLevel;

$this->title = 'Register';

?>

<style type="text/css">
    .form-horizontal .control-label {
        display: none;
    }
    .form-control {
        margin-left: 80px;
        width: 150%;
        height: 41px; 
    }

    .btn {
        height: 50px;
        width: 400px;
    }
    .has-error .help-block, .has-error .control-label, .has-error .radio, .has-error .checkbox, .has-error .radio-inline, .has-error .checkbox-inline, .has-error.radio label, .has-error.checkbox label, .has-error.radio-inline label, .has-error.checkbox-inline label {
        color: #a94442;
        margin-left: 80px;
    }
    
    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 100;
        font-size: small;
    }
    .has-error .help-block, .has-error .control-label, .has-error .radio, .has-error .checkbox, .has-error .radio-inline, .has-error .checkbox-inline, .has-error.radio label, .has-error.checkbox label, .has-error.radio-inline label, .has-error.checkbox-inline label {
        color: #cc2623;
        margin-left: 85px;
        width: 300px;
        font-size: 13px;
    }
    a{
      color: #79B9CF;
    }
    input[type="file"] {
        display: block;
        margin-left: 80px;
        background-color: white;
    }

</style>

<div class="site-login">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="text-align:left;"> 
            <h4 style="margin-left: 35px; "><b>Login</b></h4><br>
              <?php $form = ActiveForm::begin([
                'options'=> ['enctype' => 'multipart/form-data'],
                'id' => 'login-form',
                'layout' => 'horizontal',
                'enableAjaxValidation' => false
            ]); ?>

            <label style="margin-left: 80px">Name<span style="color: red">*</span></label>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true,'autofocus' => true,'autocomplete'=>"off"]) ?>

            <label style="margin-left: 80px">Email ID<span style="color: red">*</span></label>
             <?= $form->field($model, 'email')->textInput(array('placeholder' => 'Username or email')); ?>
            
            <label style="margin-left: 80px">Create Password<span style="color: red">*</span></label>
            <?= $form->field($model, 'password')->passwordInput(array('placeholder' => 'Enter password')) ?>
           
            <label style="margin-left: 80px">Mobile Number<span style="color: red">*</span></label>
            <?= $form->field($model, 'mobile')->textInput(['maxlength' => true,'placeholder'=>'Enter mobile number']) ?>
            <label style="margin-left: 80px">Total Work Experience<span style="color: red">*</span></label>

            <div class="row">
    
            <div class="col-md-5">      

            <?php 
            $workexp = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

            echo $form->field($model, 'total_work_experience')->dropDownList($workexp,
                [
                    'prompt'=>'Years'
                ]) 
            ?>
            </div>
            <div class="col-md-5">

                <?php 
                $month = array(0,1,2,3,4,5,6,7,8,9,10,11,12);

                echo $form->field($model, 'month')->dropDownList($month,
                    [
                        'prompt'=>'Month'
                    ]) 
                ?>
            </div>
            </div>
            
            <label style="margin-left: 80px">Resume<span style="color: red">*</span></label>
            <?= $form->field($model, 'file')->fileInput(); ?>
       
            <div class="form-group" style="margin-left: 80px">
              <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>                     
            
        </div>
    </div>
  </div>
</div>