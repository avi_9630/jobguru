<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DesiredCareerProfile */

$this->title = 'Update Desired Career Profile: ' . $model->desired_career_profile_id;
$this->params['breadcrumbs'][] = ['label' => 'Desired Career Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->desired_career_profile_id, 'url' => ['view', 'id' => $model->desired_career_profile_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="desired-career-profile-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
