<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DesiredCareerProfile */

$this->title = 'Create Desired Career Profile';
$this->params['breadcrumbs'][] = ['label' => 'Desired Career Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desired-career-profile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
