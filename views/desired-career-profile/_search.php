<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\DesiredCareerProfileSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="desired-career-profile-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'desired_career_profile_id') ?>

    <?= $form->field($model, 'industry_id') ?>

    <?= $form->field($model, 'department_id') ?>

    <?= $form->field($model, 'role_id') ?>

    <?= $form->field($model, 'job_type') ?>

    <?php // echo $form->field($model, 'employement_type') ?>

    <?php // echo $form->field($model, 'preferred_shift') ?>

    <?php // echo $form->field($model, 'expected_salary') ?>

    <?php // echo $form->field($model, 'desired_location') ?>

    <?php // echo $form->field($model, 'desired_industry') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
