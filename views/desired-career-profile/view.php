<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DesiredCareerProfile */

$this->title = $model->desired_career_profile_id;
$this->params['breadcrumbs'][] = ['label' => 'Desired Career Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="desired-career-profile-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->desired_career_profile_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->desired_career_profile_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'desired_career_profile_id',
            'industry_id',
            'department_id',
            'role_id',
            'job_type',
            'employement_type',
            'preferred_shift',
            'expected_salary',
            'desired_location',
            'desired_industry',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
