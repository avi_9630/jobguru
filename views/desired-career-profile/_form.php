<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DesiredCareerProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="desired-career-profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'industry_id')->textInput() ?>

    <?= $form->field($model, 'department_id')->textInput() ?>

    <?= $form->field($model, 'role_id')->textInput() ?>

    <?= $form->field($model, 'job_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'employement_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'preferred_shift')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'expected_salary')->textInput() ?>

    <?= $form->field($model, 'desired_location')->textInput() ?>

    <?= $form->field($model, 'desired_industry')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
