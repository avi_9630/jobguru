<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\DesiredCareerProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Desired Career Profiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desired-career-profile-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Desired Career Profile', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'desired_career_profile_id',
            'industry_id',
            'department_id',
            'role_id',
            'job_type',
            //'employement_type',
            //'preferred_shift',
            //'expected_salary',
            //'desired_location',
            //'desired_industry',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
