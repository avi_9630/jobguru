<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\UserLevel;
use app\models\User;

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header'=>'Sr.No'],

            // 'user_id',
            // 'level_id',
            [
                'filter' => Html::activeDropDownList($searchModel, 'level_id',
                            ArrayHelper::map(UserLevel::find()->all(),
                            'user_level_id', 'name'),
                ['class'=>'form-control','prompt' => 'Select','onchange'=>'submitm();']),

                'attribute' => 'level_id',
                'value' => 'level.name'
            ],
            'name',
            'username',
            'email:email',
            // 'password',
            // 'authKey',
            // 'status',
            [
                'attribute'=>'status',
                'filter'=>array("1"=>"Active","0"=>"Inactive"),
            ],
            'mobile',
            'total_work_experience',
            'month',
            'resume',
            [
                'attribute'=>'resume',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Yii::getAlias('@web').'/resume/'.$data->resume,['height'=>'60','width'=>'60']);
                }
            ],
            'employement_detail_id',
            'duration',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn','header' => 'Action'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<script>
    $('select option').filter(function() {
        $(this).find('option').get(0).remove();
    }).remove();
</script>