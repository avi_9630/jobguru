<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\UserLevel;
use dosamigos\datepicker\DatePicker;
use yii\helpers\Url;

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'level_id')->dropDownList(
        ArrayHelper::map(UserLevel::find()->asArray()->all(), 'user_level_id', 'name'),
        ['prompt'=>'Select User Level'])
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    
    <!-- <?= $form->field($model, 'status')->textInput() ?> -->
    <!-- <?= $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'Inactive'],
    ['prompt' => 'Select Status']); ?> -->

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_work_experience')->textInput() ?>

    <!-- <?= $form->field($model,'month')->widget(DatePicker::className(),['clientOptions' => ['defaultDate' => '2014-01-01']]) ?> -->

    <?= $form->field($model, 'resume')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'employement_detail_id')->textInput() ?> -->

    <?= $form->field($model, 'duration')->textInput(['maxlength' => true]) ?> 

    <!-- <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>  -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
