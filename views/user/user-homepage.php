<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<style type="text/css">
	.jobguru-banner-area {
    
    /*width: 1317px;*/
        margin-left: calc(50% - 50vw);
        margin-right: calc(50% - 50vw);
        /*max-width: 1000%;*/
        width: auto;
        margin-top: -20px;
    }
    .wrap {
        min-height: 100%;
        height: auto;
        margin: 0 auto -60px;
        padding: 0 0 60px;
        background-color: #EEEEEE;
    }
    .banner-search h4{
        margin-right: 784px;
    }
    label{
        display: none;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
    }
    .help-block {
        display: none;
        margin-top: 5px;
        margin-bottom: 10px;
        color: #737373;
    }
    .banner-single-slider {
        background-color: #eee;
        background-position: center center !important;
        background-size: cover !important;
        background-repeat: no-repeat !important;
        height: 300px;
    }
</style>


<div class="user-form">
    
    <section class="jobguru-banner-area">
       <div class="banner-slider owl-carousel owl-loaded owl-drag">      
       <div class="owl-stage-outer">
          <div class="owl-stage" style="transform: translate3d(-2698px, 0px, 0px); transition: all 0s ease 0s; width: 8094px;">
             <div class="owl-item cloned" style="width: 1349px;">
                <div class="banner-single-slider slider-item-1">
                   <div class="slider-offset"></div>
                </div>
             </div>
             <div class="owl-item cloned" style="width: 1349px;">
                <div class="banner-single-slider slider-item-2">
                <div class="slider-offset"></div>
                </div>
             </div>
             <div class="owl-item active" style="width: 1349px;">
                <div class="banner-single-slider slider-item-1">
                   <div class="slider-offset"></div>
                </div>
             </div>
             <div class="owl-item" style="width: 1349px;">
                <div class="banner-single-slider slider-item-2">
                <div class="slider-offset"></div>
                </div>
             </div>
             <div class="owl-item cloned" style="width: 1349px;">
                <div class="banner-single-slider slider-item-1">
                <div class="slider-offset"></div>
                </div>
             </div>             
          </div>
       </div>
       <!-- <div class="owl-nav disabled">
          <button type="button" role="presentation" class="owl-prev">
             <i class="fa fa-chevron-left"></i></button>
          <button type="button" role="presentation" class="owl-next">
             <i class="fa fa-chevron-right"></i></button>
       </div> -->
       <!-- <div class="owl-dots">
          <button role="button" class="owl-dot active"><span></span></button>
          <button role="button" class="owl-dot"><span></span></button>
       </div> -->
       </div>
       <div class="banner-text">
          <div class="container">
             <div class="row">
                <div class="col-md-12">
                   <div class="banner-search">
                   <?php $form = ActiveForm::begin(); ?>                                            
                      <!-- <form> -->
                        <h4 style="">Search Jobs</h4>
                        <div class="banner-form-box">
                            <div class="banner-form-input">
                            <?= $form->field($model, 'name')->textInput(['placeholder' =>'search By skill,company..']) ?>
                            </div>
                            <div class="banner-form-input">
                               <?= $form->field($model, 'name')->textInput(['placeholder' =>'Location..']) ?>
                            </div>
                            <div class="banner-form-input">
                               <?= $form->field($model, 'name')->textInput(['placeholder' =>'Experience..']) ?>
                            </div>
                            <div class="banner-form-input">
                               <?= $form->field($model, 'name')->textInput(['placeholder' =>'Salary..']) ?>
                            </div>
                            
                            <div class="banner-form-input">                               
                               <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>
                            </div>
                         </div>
                      <!-- </form> -->
                      <?php ActiveForm::end(); ?>
                   </div>                   
                </div>
             </div>

          </div>
       </div>
    </section>
    <br>
<style type="text/css">
    .card-title {
    padding: 32px 20px 10px;
    margin-bottom: 0;
    border-bottom: solid 1px #eee;
}
</style>

    <!-- First section.... -->
    <div class="row">
        <div>
            <h2>job</h2>&nbsp
        </div>
        <!-- Left section -->
        <div class="col-md-8">            
            <div class="row" style="background-color: white">
                <div class="card-title row">
                    <span style="color: black"><b>New Recommended Job(s)</b></span>
                </div>
            </div>
            <div class="row" style="background-color: white">
                <div class="col-md-1"></div>                
                <div class="col-md-5">
                    <div style="color: black"><b>Software developer,Yii Developer</b></div>
                </div>
                <br><br><br><br><br><br><br><br><br>
            </div>
            <div class="row" style="background-color: white">
                <div class="view-all right-align">
                    <?= Html::a('View All', ['user/recommendedjobs'], ['class' => ' btnlog','target'=>'_blank']) ?>
                    <br>
                </div>
            </div>
        </div>
        <!-- Right Sectio -->
        <div class="col-md-4">
            <div class="row" style="color: white">
                
            </div>
        </div>
    </div>
    <!-- Second Section. -->
    <br>
    <div class="row">
        <!-- Left section -->
        <div class="col-md-8">            
            <div class="row" style="background-color: white">
                <div class="card-title row">
                    <span style="color: black"><b>New Recommended Job(s)</b></span>
                </div>
            </div>
            <div class="row" style="background-color: white">
                <div class="col-md-1"></div>                
                <div class="col-md-5">
                    <div style="color: black"><b>Software developer,Yii Developer</b></div>
                </div>
                <br><br><br><br><br><br><br><br><br>
            </div>
            <div class="row" style="background-color: white">
                <div class="view-all right-align">
                    <?= Html::a('View All', ['user/recommendedjobs'], ['class' => ' btnlog','target'=>'_blank']) ?>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <!-- Third section -->
    <br>
    <div class="row">
        <!-- Left section -->
        <div class="col-md-8">            
            <div class="row" style="background-color: white">
                <div class="card-title row">
                    <span style="color: black"><b>New Recommended Job(s)</b></span>
                </div>
            </div>
            <div class="row" style="background-color: white">
                <div class="col-md-1"></div>                
                <div class="col-md-3">
                    <div>Software developer,Yii Developer</div>
                </div>
                <br><br><br><br><br><br><br><br><br>
            </div>
            <div class="row" style="background-color: white">
                <div class="view-all right-align">
                    <?= Html::a('View All', ['user/recommendedjobs'], ['class' => ' btnlog','target'=>'_blank']) ?>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <!-- Forth section -->
    <br>
    <div class="row">
        <!-- Left section -->
        <div class="col-md-8">            
            <div class="row" style="background-color: white">
                <div class="card-title row">
                    <span style="color: black"><b>New Recommended Job(s)</b></span>
                </div>
            </div>
            <div class="row" style="background-color: white">
                <div class="col-md-1"></div>                
                <div class="col-md-5">
                    <div style="color: black"><b>Software developer,Yii Developer</b></div>
                </div>
                <br><br><br><br><br><br><br><br><br>
            </div>
            <div class="row" style="background-color: white">
                <div class="view-all right-align">
                    <?= Html::a('View All', ['user/recommendedjobs'], ['class' => ' btnlog']) ?>
                    <br>
                </div>
            </div>
        </div>
    </div>

   
            
    
    
</div>

<style type="text/css">
    .view-all {
        border-top: 0;
        padding: 0 20px 20px;
        margin: 0;
        text-transform: uppercase;
    }
    .btnlog{
        float: right;
    }
</style>