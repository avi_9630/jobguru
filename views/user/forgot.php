<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\widgets\Alert;

?>

<!-- <?= Alert::widget() ?> -->

<div class="user-form">
    <br><br><br><br>
    <div class="col-lg-2"></div>

    <div class="col-lg-8">
    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'currentPassword')->passwordInput() ?>
    <?= $form->field($model, 'newPassword')->passwordInput() ?>
    <?= $form->field($model, 'newPasswordConfirm')->passwordInput() ?>

    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-10">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
</div>
<div class="col-lg-2"></div>

</div>
