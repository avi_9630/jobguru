<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\JobSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jobs';
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="job-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <!-- <p>
        <?= Html::a('Create Job', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?php Pjax::begin(); ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header'=>'Sr.No'],

            // 'job_id',
            // 'user_id',
            // [
            //     'attribute'=>'user_id',
            //     'value' => 'user.name'
            // ],            
            // 'profile_id',
            // [
            //     'attribute'=>'profile_id',
            //     'value' => 'profile.key_id'
            // ],

            [
                'attribute'=>'skill_id',
                'value' => 'skill.name'
            ],
            [
                'attribute'=>'company_id',
                'value' => 'company.name'
            ],
            [
                'attribute'=>'designation_id',
                'value' => 'designation.name'
            ],
            [
                'label' => 'Country',
                'attribute'=>'country_id',
                'value' => 'country.name'
            ],
            [
                'label' => 'State',
                'attribute'=>'state_id',
                'value' => 'state.name'
            ],
            [
                'label' => 'City',
                'attribute'=>'city_id',
                'value' => 'city.name'
            ],

            // 'title_name',
            // 'discription',
            'experience',
            'salary',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn','header'=>'Action'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
