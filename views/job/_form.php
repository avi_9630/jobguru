<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Skill;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Profile;
// use app\models\Skill;
use app\models\Designation;

/* @var $this yii\web\View */
/* @var $model app\models\Job */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-form">
    <br><br><br>    

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <?php $form = ActiveForm::begin(); ?>
<!-- 
            <?= $form->field(new Skill, 'name')->textInput(['maxlength' => true]) ?> -->

            <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(User::find()
                    ->asArray()
                    ->all(), 'user_id','name'))
            ?>

            <?= $form->field($model, 'profile_id')->dropDownList(ArrayHelper::map(Profile::find()
                    ->asArray()
                    ->all(), 'profile_id','key_id'))
            ?>
            <!-- <?= $form->field($model, 'profile_id')->textInput() ?> -->

            <?= $form->field($model, 'skill_id')->dropDownList(ArrayHelper::map(Skill::find()
                    ->asArray()
                    ->all(), 'skill_id','name'))
            ?>

            <?= $form->field($model, 'designation_id')->dropDownList(ArrayHelper::map(Designation::find()
                    ->asArray()
                    ->all(), 'designation_id','name'))
            ?>

            <?= $form->field($model, 'title_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'discription')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'experience')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'salary')->textInput(['maxlength' => true]) ?>

            <!-- <?= $form->field($model, 'created_at')->textInput() ?>

            <?= $form->field($model, 'updated_at')->textInput() ?>
 -->
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
        <div class="col-md-2"></div>
    </div>

</div>
