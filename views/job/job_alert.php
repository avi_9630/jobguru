<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Skill;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Profile;
use app\models\Designation;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Job */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css">
	.select2-container .select2-selection--single {
	    height: 34px; 
	}
</style>

<div class="job-form">
	<br><br>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?php $form = ActiveForm::begin(); ?>

            <?php 
            $data = ArrayHelper::map(Skill::find()->asArray()->all(), 'skill_id','name');
            echo $form->field(new Skill, 'name')->widget(Select2::classname(), [
			    'data' => $data,
			    'language' => 'en',
			    'options' => ['placeholder' => 'Skills,Designation,Roles,Companies'],
			    'pluginOptions' => [
			        'allowClear' => true
			    ],
			]);

            ?>
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
        <div class="col-md-2"></div>
    </div>
</div>
