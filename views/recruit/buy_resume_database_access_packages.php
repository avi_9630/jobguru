<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\RecruitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recruits';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recruit-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Recruit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'recruit_id',
            'email',
            'password',
            'company_name',
            'industry_id',
            //'type',
            //'super_user',
            //'designation',
            //'office_address',
            //'pincode_id',
            //'isd',
            //'std',
            //'std_number',
            //'country_code',
            //'mobile_number',
            //'alternate_email:email',
            //'terms_conditions',
            'photo',
            [
                'attribute'=>'photo',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Yii::getAlias('@web').'/upload/'.$data->photo,['height'=>'60','width'=>'60']);
                }
            ],
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
