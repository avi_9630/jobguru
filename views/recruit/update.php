<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recruit */

$this->title = 'Update Recruit: ' . $model->recruit_id;
$this->params['breadcrumbs'][] = ['label' => 'Recruits', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->recruit_id, 'url' => ['view', 'id' => $model->recruit_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recruit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
