<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\UserLevel;
use app\models\Industry;
use app\models\Country;
use app\models\State;
use app\models\City;
use app\models\Pincode;


$this->title = 'Register';

?>
<style type="text/css">
    .form-horizontal .control-label {
        display: none;
    }
    .form-control {
        margin-left: 80px;
        width: 150%;
        height: 41px; 
    }

    .btn {
        height: 50px;
        width: 400px;
        margin-left: 97px;
    }
    .has-error .help-block, .has-error .control-label, .has-error .radio, .has-error .checkbox, .has-error .radio-inline, .has-error .checkbox-inline, .has-error.radio label, .has-error.checkbox label, .has-error.radio-inline label, .has-error.checkbox-inline label {
        color: #a94442;
        margin-left: 80px;
    }
    
    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 100;
        font-size: small;
    }
</style>

<div class="site-login">    
    <div class="modal-dialog"> 
    <div class="modal-content">
    <div class="modal-body" style="text-align:left;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 style="margin-left: 35px; "><b>Login</b></h4><br>
        <?php $form = ActiveForm::begin([
            // 'options' => ['enctype' => 'multipart/form-data'],
            'id' => 'login-form',
            'layout' => 'horizontal',
            // 'enableAjaxValidation' => false
        ]); ?>
    
            <label style="margin-left: 80px">Email ID/Username<span style="color: red">*</span></label>
            <?= $form->field($model, 'email')->textInput(array('placeholder' => 'Username or email')); ?>

            <label style="margin-left: 80px">Password<span style="color: red">*</span></label>
            <?= $form->field($model, 'password')->passwordInput(array('placeholder' => 'Password')) ?>

            <label style="margin-left: 80px">Company Name<span style="color: red">*</span></label>
            <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

            <label style="margin-left: 80px">Industry<span style="color: red">*</span></label>
            
            <?= $form->field($model, 'industry_id')->dropDownList(ArrayHelper::map(Industry::find()->asArray()->all(),'industry_id', 'name'),['prompt'=>'Select Industry']) ?>

            <label style="margin-left: 80px">Type<span style="color: red">*</span></label>
            <!-- <?= $form->field($model, 'type')->checkboxList(['maxlength' => true]) ?> -->

            <?php $list = [0 => 'Company', 1 => 'Consultant'];
                echo $form->field($model, 'type')->dropDownList($list,['prompt'=>'Select your choice']);  ?>


            <label style="margin-left: 80px">Super User<span style="color: red">*</span></label>
            <?= $form->field($model, 'super_user')->textInput(['maxlength' => true,
            'placeholder' => 'Contact person name']) ?>

            <label style="margin-left: 80px">Designation<span style="color: red">*</span></label>
            <?= $form->field($model, 'designation')->textInput(['maxlength' => true,
            'placeholder' => 'Designation..']) ?>

            <label style="margin-left: 80px">Office Address<span style="color: red">*</span></label>
            <?= $form->field($model, 'office_address')->textInput(['maxlength' => true,
            'placeholder' => 'Office Address']) ?>


<!-- ****************************************************************************************** -->
            
            <label style="margin-left: 80px">Country<span style="color: red">*</span></label>

            <?php
            echo $form->field(new Country(), 'country_id')->dropDownList(
                ArrayHelper::map(Country::find()->all(), "country_id", "name"),
                [
            'prompt' => 'Select Country',
            'id' => 'country_id',
            'onchange' => 'getAllState()',
                ]
            )->label("Country");
            ?>

            <label style="margin-left: 80px">State<span style="color: red">*</span></label>

            <?php
            echo $form->field(new State(), 'state_id')->dropDownList(
                ArrayHelper::map(State::find()->all(), "state_id", "name"),
                [
            'prompt' => 'Select State',
            'id' => 'depState',
            'onchange' => 'getAllCity()',
                ]
            )->label("State");
            ?>

            <label style="margin-left: 80px">City<span style="color: red">*</span></label>

            <?php
            echo $form->field(new City(), 'city_id')->dropDownList(
                ArrayHelper::map(City::find()->all(), "city_id", "name"),
                [
            'prompt' => 'Select City',
            'id' => 'depCity',
            'onchange' => 'getAllPincode()',
                ]
            )->label("City");
            ?>

            <label style="margin-left: 80px">Pincode<span style="color: red">*</span></label>

            <?= $form->field($model, 'pincode_id')->dropDownList(ArrayHelper::map(Pincode::find()->all(),'pincode_id','picode_number'),['prompt'=>'Select Pincode','id' => 'depPincode']) ?>

            <!-- ******************************************************************* -->

            <label style="margin-left: 80px">ISD<span style="color: red">*</span></label>
            <?= $form->field($model, 'isd')->textInput() ?>

            <label style="margin-left: 80px">STD<span style="color: red">*</span></label>
            <?= $form->field($model, 'std')->textInput() ?>

            <label style="margin-left: 80px">STD Number<span style="color: red">*</span></label>
            <?= $form->field($model, 'std_number')->textInput() ?>

            <label style="margin-left: 80px">Country Code<span style="color: red">*</span></label>
            <?= $form->field($model, 'country_code')->textInput() ?>

            <label style="margin-left: 80px">Mobile Number<span style="color: red">*</span></label>
            <?= $form->field($model, 'mobile_number')->textInput() ?>

            <label style="margin-left: 80px">Alternate Email<span style="color: red">*</span></label>
            <?= $form->field($model, 'alternate_email')->textInput(['maxlength' => true]) ?>

            <label style="margin-left: 80px">Termas <b>&&</b>Conditions <span style="color: red">*</span></label>
            <?= $form->field($model, 'terms_conditions')->textInput(['maxlength' => true]) ?>

            <label style="margin-left: 80px">Photo<span style="color: red">*</span></label>
            <?= $form->field($model, 'file')->fileInput(['maxlength' => true]) ?>
                
            <div class="form-group">
                <?= Html::submitButton('Register', ['class' => ' btn btn-primary btnRegister']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
    </div>
    </div>
</div>




<script>
$(document).ready(function(){
 
})
function getAllState(){
    var country_id = document.getElementById("country_id").value;
    
    $.ajax({
        url: '<?php echo Url::to(["recruit/lists"]); ?>',
        type: 'post',
        data: {
            type : 1,
            id : country_id
        },
        success: function (res) {
            var nandghar = JSON.parse(res);
            var areaOption = "<option value=''>Select State</option>";
             for (var i = 0; i < nandghar.length; i++) {
                 areaOption += '<option value="' + nandghar[i]['state_id'] + '">' + nandghar[i]['name'] + '</option>'
             }
            $("#depState").html(areaOption);

            if(document.getElementById("depState") !== null){
                $("#depState").val($("#depState").val()).change();
            }
        },
        error: function (res) {
        }
    }); 
}


    function getAllCity(){
        var state_id = document.getElementById("state_id").value;
            
            $.ajax({
                url: '<?php echo Url::to(["recruit/lists"]); ?>',
                type: 'post',
                data: {
                    type : 2,
                    id : state_id
                },
                success: function (res) {
                var nandghar = JSON.parse(res);
                var areaOption = "<option value=''>Select City</option>";
                 for (var i = 0; i < nandghar.length; i++) {
                     areaOption += '<option value="' + nandghar[i]['city_id'] + '">' + nandghar[i]['name'] + '</option>'
                 }
                $("#depCity").html(areaOption);
                    if(document.getElementById("depCity") !== null){
                        $("#depCity").val($("#depCity").val()).change();
                    
                    }
                },
                error: function (res) {
                }
            }); 
    }

    function getAllPincode(){
        var city_id = document.getElementById("city_id").value;
            
            $.ajax({
                url: '<?php echo Url::to(["recruit/lists"]); ?>',
                type: 'post',
                data: {
                    type : 3,
                    id : city_id
                },
                success: function (res) {
                var nandghar = JSON.parse(res);
                var areaOption = "<option value=''>Select Pincode</option>";
                 for (var i = 0; i < nandghar.length; i++) {
                     areaOption += '<option value="' + nandghar[i]['pincode_id'] + '">' + nandghar[i]['name'] + '</option>'
                 }
                $("#depPincode").html(areaOption);
                    if(document.getElementById("depPincode") !== null){
                        $("#depPincode").val($("#depPincode").val()).change();
                    
                    }
                },
                error: function (res) {
                }
            }); 
    }
</script>