<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Recruit */

$this->title = $model->recruit_id;
$this->params['breadcrumbs'][] = ['label' => 'Recruits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="recruit-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->recruit_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->recruit_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'recruit_id',
            'email:email',
            'password',
            'company_name',
            'industry_id',
            'type',
            'super_user',
            'designation',
            'office_address',
            'pincode_id',
            'isd',
            'std',
            'std_number',
            'country_code',
            'mobile_number',
            'alternate_email:email',
            'terms_conditions',
            'photo',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
