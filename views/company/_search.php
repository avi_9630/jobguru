<?php

use yii\helpers\Html;
// use yii\bootstrap\ActiveForm;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\icons\Icon;
Icon::map($this);

/* @var $this yii\web\View */
/* @var $model app\models\search\CompanySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- *******************CSS for Banner*************************** -->

<style type="text/css">
   .jobguru-banner-area {
    
    /*width: 1317px;*/
        margin-left: calc(50% - 50vw);
        margin-right: calc(50% - 50vw);
        /*max-width: 1000%;*/
        width: auto;
        margin-top: -20px;
    }
    .wrap {
        min-height: 100%;
        height: auto;
        margin: 0 auto -60px;
        padding: 0 0 60px;
        background-color: #EEEEEE;
    }
    .banner-search h4{
        margin-right: 784px;
    }
    label{
        display: none;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
    }
    .help-block {
        display: none;
        margin-top: 5px;
        margin-bottom: 10px;
        color: #737373;
    }
    .banner-single-slider {
        background-color: #eee;
        background-position: center center !important;
        background-size: cover !important;
        background-repeat: no-repeat !important;
        height: 300px;
    }
    .section_70 {
        padding: 24px 0;
    }
    .section_70 {
        padding: 24px 0;
        margin-right: -30px;
    }
</style>
<style type="text/css">
    .card-title {
        padding: 32px 20px 10px;
        margin-bottom: 0;
        border-bottom: solid 1px #eee;
    }
    .svg-inline--fa {
        color: #1E8D4E;
    }
</style>
<style type="text/css">
    .jobguru-inner-hire-area {
        background:  <?php echo Url::to('@web/images/hire.jpg'); ?> !important;
        position: relative;
        z-index: 1;
    } 
</style>
<style type="text/css">
    .wrap {
        background-color: #F4F4F4;
    }
    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
        background-color: white;
    }
</style>
<style type="text/css">
    .how-works-box {
        text-align: center;
        position: relative;
        border-radius: 50%;
        height: 250px;
        padding: 75px 30px;
        margin: 70px auto 0;
        width: 250px;
        z-index: 1;
        background: #07474e none repeat scroll 0 0;
    }
</style>
<style type="text/css">
    .banner-form-input input {
        padding: 5px 10px;
        width: 100%;
        border: medium none;
        width: 711px;
        height: 42px;
    }
    .form-control {
        display: block;
        width: 100%;
        height: 40px;
    }
    .btn-primary {
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
        height: 40px;
    }
</style>

<!-- ************************************************************ -->

<div class="company-search">

    <!--*************Banner of index........  -->
    <section class="jobguru-banner-area" >
        <div class="banner-slider owl-carousel owl-loaded owl-drag" >      
            <div class="owl-stage-outer">
                <div class="owl-stage" style="transform: translate3d(-2698px, 0px, 0px); transition: all 0s ease 0s; width: 8094px;">
                    <div class="owl-item cloned" style="width: 1349px;">
                        <div class="banner-single-slider slider-item-1">
                          <div class="slider-offset"></div>
                        </div>
                    </div>
                    <div class="owl-item cloned" style="width: 1349px;">
                        <div class="banner-single-slider slider-item-2">
                        <div class="slider-offset"></div>
                        </div>
                    </div>
                    <div class="owl-item active" style="width: 1349px;">
                        <div class="banner-single-slider slider-item-1">
                          <div class="slider-offset"></div>
                        </div>
                    </div>
                    <div class="owl-item" style="width: 1349px;">
                        <div class="banner-single-slider slider-item-2">
                        <div class="slider-offset"></div>
                        </div>
                    </div>
                    <div class="owl-item cloned" style="width: 1349px;">
                        <div class="banner-single-slider slider-item-1">
                        <div class="slider-offset"></div>
                        </div>
                    </div>
                    <div class="owl-item cloned" style="width: 1349px;">
                        <div class="banner-single-slider slider-item-2">
                        <div class="slider-offset"></div>
                        </div>
                    </div>
                </div>
            </div>   
       </div>
       <div class="banner-text">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <?php $form = ActiveForm::begin(['action' => ['index'],
                        'method' => 'get', 'options' => ['data-pjax' => 1 ],]); ?>
                      <!--   <form> -->
                            <!-- <div class="banner-form-box"> -->
                                <!-- <div class="banner-form-input"> -->
                                    <!-- <input type="text" placeholder="Job Title, Keywords, or Phrase"> -->
                                    <?= $form->field($model, 'name')->textInput(['placeholder'=> 'Write Company name'])->label('Your Name'); ?>

                                <!-- </div> -->
                                <!-- <div class="banner-form-input"> -->
                                    <!-- <button type="submit">Search</button> -->
                                    <!-- <button type="submit"><i class="fa fa-search"></i></button> -->
                                    
                                <!-- </div> -->
                                
                            <!-- </div> -->
                        <!-- </form> -->
                       

                    <!-- </div> -->
                    </div>
                    <div class="col-md-1">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="col-md-5"></div>                
                </div>
            </div>
        </div>
    </section>
    <br>

    <div class="row">
        <div class="col-md-7"></div>
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['placeholder'=> 'Write Company name'])->label('Your Name'); ?>
        </div>
        <div class="col-md-1">
            <?= Html::submitButton(Icon::show('search', ['class'=>'fa-1x'])) ?>
        </div>       
    </div>
<?php ActiveForm::end(); ?>
    <!--(*******************************************************) -->
</div>