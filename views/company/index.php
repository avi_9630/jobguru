<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Companies';

?>

<div class="company-index">
   
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <!-- <p>
        <?= Html::a('Create Company', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
    
    <?php Pjax::begin(); ?>

        <?php  echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,

        'summary'=>'',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header'=>'Sr.No'],

            // 'company_id',
            // 'user_id',
            // [
            //     'attribute'=>'user_id',
            //     'value'=>'user.name'
            // ],
            // 'name',
            [
            'attribute' => 'name',
            'filterInputOptions' => [
                'class'       => 'form-control',
                'placeholder' => 'Type company name...',
             ]
        ],
            // 'about_company',
            // 'rating',
            //'review',
            //'created_at',
            //'updated_at',

            // ['class' => 'yii\grid\ActionColumn','header'=>'Action'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>
</div>
<div class="col-md-1"></div>

