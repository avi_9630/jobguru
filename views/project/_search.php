<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\ProjectSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'project_id') ?>

    <?= $form->field($model, 'project_title') ?>

    <?= $form->field($model, 'client') ?>

    <?= $form->field($model, 'project_status') ?>

    <?= $form->field($model, 'working_from') ?>

    <?php // echo $form->field($model, 'worked_till') ?>

    <?php // echo $form->field($model, 'project_detail') ?>

    <?php // echo $form->field($model, 'project_location') ?>

    <?php // echo $form->field($model, 'project_site') ?>

    <?php // echo $form->field($model, 'employement') ?>

    <?php // echo $form->field($model, 'team_size') ?>

    <?php // echo $form->field($model, 'role_id') ?>

    <?php // echo $form->field($model, 'skill_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
