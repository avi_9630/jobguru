<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Role;
use app\models\Skill;
use kartik\select2\Select2

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'project_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'client')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'project_status')->radioList(array('1'=>'In Progress', 2=>'Finished')); ?>

    <div class="col-md-6">
        <?php echo $form->field($model, 'working_from')->dropDownList($model->getYearsList(),
            [
            'id' => 'team-size',
            'prompt'=>'Select Year '
        ]
    ); ?>
    </div>
    <div class="col-md-6">
        <?php 

        $monthList = array( 1=>'Jauary',
                            2=>'February',
                            3=>'march',
                            4=>'April' ,
                            5=>'May',
                            6=>'Jun',
                            7=>'July',
                            8=>'August',
                            9=>'September',
                            10=>'October',
                            11=>'November',
                            12=>'Decempber'
                        );

        echo $form->field($model, 'month')->dropDownList($monthList,
        [
            'id' => 'team-size',
            'prompt'=>'Select Month '
        ]);

        ?>
    </div>

    <?php echo $form->field($model, 'worked_till')->dropDownList(['a' => 'Present']); ?>

    <?= $form->field($model, 'project_detail')->textArea(['rows' => 6 ,'maxlength' => true]) ?>

    <?= $form->field($model, 'project_location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'project_site')->radioList(array(1 =>'Offsite', 2 =>'Onsite')); ?>

    <?= $form->field($model, 'employement')->radioList(array('1'=>'Full Time ',2=>'Part Time' , 3 => 'Contractual')); ?>

    <?php 
    $teamSize = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40);


    echo $form->field($model, 'team_size')->dropDownList($teamSize,
    [
        'id' => 'team-size',
        'prompt'=>'Select Team Size '
    ]);

   ?>


    <?= $form->field($model, 'role_id')->dropDownList(ArrayHelper::map(Role::find()->all(),
        'role_id','name'),
        ['prompt'=>'Select Role']) 
    ?>

    <?= $form->field($model, 'skill_id')->dropDownList(ArrayHelper::map(Skill::find()->all(),
        'skill_id','name'),
        [
            'multiple' => true,
            'prompt'=>'Skill used..'
        ])?>
    <?=
       // $skill = Skill::find()->select(['skill_id','name'])->asArray()->all();

    Select2::widget([
        'name' => 'state_10',
        'model' => $model,
        'options' => [
            'placeholder' => 'Select provinces ...',
            'multiple' => true
        ],
    ])
    ?>

     <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
