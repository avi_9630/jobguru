<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\UserLevel;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FA;
use kartik\icons\Icon;
Icon::map($this);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<style type="text/css">

  .navbar-inverse {
    background-color: white;
    border-color: black;
    
  }
  .navbar-nav > li {
      float: left;
      margin-right: 20px;
  }

  .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover,.navbar-inverse .navbar-nav > .active > a:focus {
      color: #fff;
      background-color: white; 
  }

  .navbar-inverse .navbar-nav > li > a {
        color: #272424;
  }
  .caret {
    display: none;
    width: 0;
    height: 0;
    margin-left: 2px;
    vertical-align: middle;
    
    border-right: 4px solid transparent;
    border-left: 4px solid transparent;
  }
  .navbar-brand {
    float: left;
    height: 74px;
    padding: 15px 15px;
    font-size: 18px;
    line-height: 39px;
  } 
  .navbar-nav {
    float: right;
    margin: 0;
  }  

  ul, ol {
    margin: 0;
    padding: 13px;
  }

    .navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:hover, .navbar-inverse .navbar-nav > .open > {
      color: #000;
      background-color: #ffffff;
  }
</style>

<body >

<?php $this->beginBody() ?>

<div class="wrap">
  
  <?php if(Yii::$app->user->isGuest) { ?>
  <?php
  
    NavBar::begin([
      'brandLabel' =>Html::img('@web/images/guru1.png',['style'=> 'width: 200px; height: 50px;']),
      'brandUrl' => ['/site/index'],
      'options' => ['class' => 'navbar-inverse navbar-fixed-top nav-color'],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'nav navbar-nav navbar-main-menu'],
        'encodeLabels' => false,

        'items' => [
                [
                    'label' => 'JOBS',
                    'items' => [
                        [
                            'label' => 'Search Jobs',
                            'url' => ['/job/index/'],
                            'linkOptions'=>['target'=>'_blank'],
                        ],
                        [
                            'label' => 'Advance Job Search',
                            'url' => ['/job/advance-search/'],
                            'linkOptions'=>['target'=>'_blank'],
                        ],
                        [
                            'label' => 'Create Free Job Alert',
                            'url' => ['/job/job-alert/'],
                            'linkOptions'=>['target'=>'_blank'],
                        ],                        
                        [
                            'label' => 'Register Now',
                            'url' => ['/site/register/'],
                            'linkOptions'=>['target'=>'_blank'],
                        ],
                    ],

                    ['label' => 'Advance Search', 'url' => ['/job/advance-search']],
                ],

                [
                    'label' => 'COMPANIES',
                    'items' => [
                        [
                          'label' => ' Browse All Companies',
                          'url' =>     ['/company/index/'],
                          'linkOptions'=>['target'=>'_blank'],
                        ],

                        [
                          'label' => ' Abot Companies',
                          'url' => ['/compa ny/index/'],
                          'linkOptions'=>['target'=>'_blank'],
                        ],                            
                    ],
                ],
                [
                    'label' => 'SERVICES',
                    'items' => [
                        ['label' => 'Resume writing', 'url' => ['/resume/index/']],
                        // ['label' => 'Advance Search', 'url' => ['/job/advance-search']],
                    ],
                ],

                [
                    'label' => 'Masters',
                    'items' => [
                        ['label' => 'City', 'url' => ['/city/index/']],
                        // ['label' => 'Company', 'url' => ['/company/index/']],
                        ['label' => 'Country ', 'url' => ['/country/index/']],
                        ['label' => 'Course', 'url' => ['/course/index/']],
                        ['label' => 'Desired Career Profile', 'url' => ['/desired-career-profile/index/']],
                        ['label' => 'Education', 'url' => ['/education/index/']],
                        ['label' => 'Functional Area ', 'url' => ['/functional-area/index/']],
                        ['label' => 'Industry', 'url' => ['/industry/index/']],
                        ['label' => ' Online Profile', 'url' => ['/online-profile/index/']],
                        ['label' => 'Personal Details', 'url' => ['/personal-detail/index/']],
                        ['label' => 'Pincode', 'url' => ['/pincode/index/']],
                        ['label' => 'Profile ', 'url' => ['/profile/index/']],
                        ['label' => 'Project ', 'url' => ['/project/index/']],
                        ['label' => 'Qualification ', 'url' => ['/qualification/index/']],
                        ['label' => 'skill ', 'url' => ['/skill/index/']],
                        ['label' => 'Specialization ', 'url' => ['/specialization/index/']],
                        ['label' => 'State ', 'url' => ['/state/index/']],
                        ['label' => 'Employement ', 'url' => ['/employement-detail/index/']],
                        

                    ],
                ],
                [
                    'label' => 'MORE',
                    'items' => [
                        [
                            'label' => 'Carrer Tips',
                            
                        ],                                
                    ],
                ],
                [
                  'label' => 'Login',
                  'url' => ['/site/login'],
                  // 'options'=> ['class'=>'login'],
                  // 'linkOptions'=>[
                  //   // 'class'=>'login-modal',
                  //   'onclick'=>'login();return false;',
                  //   'data-toggle' =>'modal',
                  //   'data-target' =>'#myModal'
                  // ],
                ],

                [
                  'label' => Html::img('@web/images/notification.png',['style'=> 'width: 20px; height: 20px;']),
                  // FA::icon('home'),
                              // ->spin()
                              // ->fixedWidth()
                              // ->pullLeft()
                              // ->size(FA::SIZE_LARGE),

                  // '<i class="fas fa-bell"></i>',

                  'url' => ['/site/index'],
                ],

                [
                  'label' => 'FOR EMPLOYERS',
                  'items' =>[
                      [
                          'label' => 'Buy Online ',
                          'url' => ['/resume/index/'],
                        'linkOptions' => ['target' => '_blank',
                          ],
                      ],

                      [
                          'label' => 'Employer Login ',
                          'url' => ['/recruit/login/'],
                          'linkOptions' => [
                              'target'=> '_blank',
                          ],
                      ],
                  ],
                ],

        ],
    ]);
    
    NavBar::end();

    }else{

      $userLevel = UserLevel::find()->where(['user_level_id' => Yii::$app->user->identity['level_id']])->one();
      $userName = Yii::$app->user->identity['name'];

    NavBar::begin([
        'brandLabel' => '<b>JOBGURU</b>',
        // 'brandUrl' => Yii::$app->homeUrl,
        'brandUrl' => ['/user/user-homepage'],
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top nav-color',
        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => [

            [
                'label' => 'JOBS',
                'items' => [
                    ['label' => 'Search Jobs', 'url' => ['/job/index/']],
                    ['label' => 'Advance Search', 'url' => ['/job/advance-search']],
                ],
            ],
            [
                'label' => 'COMPANIES',
                'items' => [
                    ['label' => 'All Companies', 'url' => ['/company/index/']],
                    // ['label' => 'Advance Search', 'url' => ['/job/advance-search']],
                ],
            ],
            [
                'label' => 'SERVICES',
                'items' => [
                    ['label' => 'Resume writing', 'url' => ['/resume/index/']],
                    // ['label' => 'Advance Search', 'url' => ['/job/advance-search']],
                ],
            ],
            [
                'label' => 'Masters',
                'items' => [
                    ['label' => 'City', 'url' => ['/city/index/']],
                    // ['label' => 'Company', 'url' => ['/company/index/']],
                    ['label' => 'Country ', 'url' => ['/country/index/']],
                    ['label' => 'Course', 'url' => ['/course/index/']],
                    ['label' => 'Desired Career Profile', 'url' => ['/desired-career-profile/index/']],
                    ['label' => 'Education', 'url' => ['/education/index/']],
                    ['label' => 'Functional Area ', 'url' => ['/functional-area/index/']],
                    ['label' => 'Industry', 'url' => ['/industry/index/']],
                    ['label' => ' Online Profile', 'url' => ['/online-profile/index/']],
                    ['label' => 'Personal Details', 'url' => ['/personal-detail/index/']],
                    ['label' => 'Pincode', 'url' => ['/pincode/index/']],
                    ['label' => 'Profile ', 'url' => ['/profile/index/']],
                    ['label' => 'Project ', 'url' => ['/project/index/']],
                    ['label' => 'Qualification ', 'url' => ['/qualification/index/']],
                    ['label' => 'skill ', 'url' => ['/skill/index/']],
                    ['label' => 'Specialization ', 'url' => ['/specialization/index/']],
                    ['label' => 'State ', 'url' => ['/state/index/']],

                ],
            ],
            [
                'label' => 'MORE',
                'items' => [
                    [
                        'label' => 'Carrer Tips',
                        
                    ],                                
                ],
            ],
            
            // ['label' => 'User', 'url' => ['/user/index']],

            [
                'label' => Html::img('@web/images/notification.png',['style'=> 'width: 20px; height: 20px;']),
                'items' => [
                    [
                        'label' => 'Carrer Tips',
                        
                    ],                                
                ],
            ],
            [
              'label' => 'MY NAUKRI',
              'items' =>[
                  [
                      'label' => 'Edit Profile',
                      'url' => ['/user/user-profile/'],
                  ],
                  [
                      'label' => 'Logout ('.$userName.':'.$userLevel['name'].')', 
                      'url' => ['/site/logout']
                  ],
              ],                            
            ],
        ],
    ]);

    NavBar::end();
  }
  ?>

  <div class="container">
      <?= Alert::widget() ?>
      <?= $content ?>
  </div>
  </div>
    
  <!-- Footer Start -->
    <footer class="jobguru-footer-area">
             <div class="footer-top section_50">
                <div class="container">
                   <div class="row">
                      <div class="col-lg-3 col-md-6">
                         <div class="single-footer-widget">
                            <div class="footer-logo">
                               <a href="index.html">
                               <img src="<?php echo Url::to('@web/images/logo3.png'); ?>" alt="jobguru logo">
                               </a>
                            </div>
                            <p>Aliquip exa consequat dui aut repahend vouptate elit cilum fugiat pariatur lorem dolor cit amet consecter adipisic elit sea vena eiusmod nulla</p>
                            <ul class="footer-social">
                               <li><a href="#" class="fb"><i class="fa fa-facebook"></i></a></li>
                               <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                               <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                               <li><a href="#" class="gp"><i class="fa fa-google-plus"></i></a></li>
                               <li><a href="#" class="skype"><i class="fa fa-skype"></i></a></li>
                            </ul>
                         </div>
                      </div>
                      <div class="col-lg-3 col-md-6">
                         <div class="single-footer-widget">
                            <h3>recent post</h3>
                            <div class="latest-post-footer clearfix">
                               <div class="latest-post-footer-left">
                                  <img src="<?php echo Url::to('@web/images/logo.png'); ?>" alt="post">
                               </div>
                               <div class="latest-post-footer-right">
                                  <h4><a href="#">Website design trends for 2019</a></h4>
                                  <p>May 16 - 2019</p>
                               </div>
                            </div>
                            <div class="latest-post-footer clearfix">
                               <div class="latest-post-footer-left">
                                  <img src="<?php echo Url::to('@web/images/logo.png'); ?>" alt="post">
                               </div>
                               <div class="latest-post-footer-right">
                                  <h4><a href="#">UI experts and modern designs</a></h4>
                                  <p>May 16 - 2019</p>
                               </div>
                            </div>
                         </div>
                      </div>
                      <div class="col-lg-3 col-md-6">
                         <div class="single-footer-widget">
                            <h3>main links</h3>
                            <ul>
                               <li><a href="#"><i class="fa fa-angle-double-right "></i> About Waxen Tech</a></li>
                               <li><a href="#"><i class="fa fa-angle-double-right "></i> Delivery Information</a></li>
                               <li><a href="#"><i class="fa fa-angle-double-right "></i> Terms &amp; Conditions</a></li>
                               <li><a href="#"><i class="fa fa-angle-double-right "></i> Customer support</a></li>
                               <li><a href="#"><i class="fa fa-angle-double-right "></i> Contact with an expert</a></li>
                               <li><a href="#"><i class="fa fa-angle-double-right "></i> community updates</a></li>
                               <li><a href="#"><i class="fa fa-angle-double-right "></i> upcoming updates</a></li>
                            </ul>
                         </div>
                      </div>
                      <div class="col-lg-3 col-md-6">
                         <div class="single-footer-widget footer-contact">
                            <h3>Contact Info</h3>
                            <p><i class="fa fa-map-marker"></i> Noida sec-132, India </p>
                            <p><i class="fa fa-phone"></i> 9654452911</p>
                            <p><i class="fa fa-envelope"></i> avinashmca94@gmail.com</p>
                            <p><i class="fa fa-envelope"></i> avinashmca94@gmail.com</p>
                            <p><i class="fa fa-fax"></i> 9654452911 </p>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
             <div class="footer-copyright">
                <div class="container">
                   <div class="row">
                      <div class="col-lg-12">
                         <div class="copyright-left">
                            <p>Copyright © 2019 . Waxen Tech Pvt Ltd</p>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
    </footer>
  <!-- ************************************* -->
  

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<div class="modal fade" id="myModal" tabindex="-1"
      role="dialog" aria-labelledby="myModalLabel">
</div>

<script>
   function login(){
      $.ajax({
          type:'POST',
          url:'site/login',
          success: function(data)
               {
                   $('#myModal').html(data);
                   $('#myModal').modal();
               }
      });
   }
</script>