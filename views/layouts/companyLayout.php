<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\UserLevel;
use yii\bootstrap\Modal;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>

<?php $this->beginBody() ?>

<div class="wrap">  
  
  <section class="jobguru-banner-area">
   <div class="banner-slider owl-carousel owl-loaded owl-drag">      
   <div class="owl-stage-outer">
      <div class="owl-stage" style="transform: translate3d(-2698px, 0px, 0px); transition: all 0s ease 0s; width: 8094px;">
         <div class="owl-item cloned" style="width: 1349px;">
            <div class="banner-single-slider slider-item-1">
               <div class="slider-offset"></div>
            </div>
         </div>
         <div class="owl-item cloned" style="width: 1349px;">
            <div class="banner-single-slider slider-item-2">
            <div class="slider-offset"></div>
            </div>
         </div>
         <div class="owl-item active" style="width: 1349px;">
            <div class="banner-single-slider slider-item-1">
               <div class="slider-offset"></div>
            </div>
         </div>
         <div class="owl-item" style="width: 1349px;">
            <div class="banner-single-slider slider-item-2">
            <div class="slider-offset"></div>
            </div>
         </div>
         <div class="owl-item cloned" style="width: 1349px;">
            <div class="banner-single-slider slider-item-1">
            <div class="slider-offset"></div>
            </div>
         </div>
         <div class="owl-item cloned" style="width: 1349px;">
            <div class="banner-single-slider slider-item-2">
            <div class="slider-offset"></div>
            </div>
         </div>
      </div>
   </div>
   <div class="owl-nav disabled">
      <button type="button" role="presentation" class="owl-prev">
         <i class="fa fa-chevron-left"></i></button>
      <button type="button" role="presentation" class="owl-next">
         <i class="fa fa-chevron-right"></i></button>
   </div>
   <div class="owl-dots">
      <button role="button" class="owl-dot active"><span></span></button>
      <button role="button" class="owl-dot"><span></span></button>
   </div>
   </div>
   <div class="banner-text">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="banner-search">
                  <h2>Hire expert freelancers.</h2>
                  <h4>We have 1542 job offers for you! </h4>
                  <form>
                     <div class="banner-form-box">
                        <div class="banner-form-input">
                           <input type="text" placeholder="Job Title, Keywords, or Phrase">
                        </div>
                        <div class="banner-form-input">
                           <input type="text" placeholder="City, State or ZIP">
                        </div>
                        <div class="banner-form-input">
                           <select class="banner-select select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                              <option selected="" data-select2-id="3">Select Sector</option>
                              <option value="1">Design &amp; multimedia</option>
                              <option value="2">Programming &amp; tech</option>
                              <option value="3">Accounting/finance</option>
                              <option value="4">content writting</option>
                              <option value="5">Training</option>
                              <option value="6">Digital Marketing</option>
                           </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="2" style="width: 161px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-cz71-container"><span class="select2-selection__rendered" id="select2-cz71-container" role="textbox" aria-readonly="true" title="Select Sector">Select Sector</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div>
                        <div class="banner-form-input">
                           <button type="submit"><i class="fa fa-search"></i></button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
  </section>

    <div class="container">
          <?= Alert::widget() ?>
          <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
