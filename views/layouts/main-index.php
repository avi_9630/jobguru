<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\IndexAsset;
use app\models\UserLevel;
use yii\bootstrap\Modal;


IndexAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
        if(Yii::$app->user->isGuest) {
    ?>
    <?php
        NavBar::begin([
            'brandLabel' => //Html::img('@web/images/guru.png',['id' => 'logo']),
            '<b>JOBGURU</b>',
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top nav-color',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right btn-back'],
                'items' => [
                        [
                          'label' => 'JOBS',
                          'items' => [
                              [
                                'label' => 'Search Jobs',
                                'url' => ['/job/index/'],
                                'linkOptions'=>[
                                              'target'=>'_blank',
                                                ],
                                ],
                            ],

                            ['label' => 'Advance Search', 'url' => ['/job/advance-search']],
                        ],

                        [
                            'label' => 'COMPANIES',
                            'items' => [
                                [
                                  'label' => 'All Companies',
                                  'url' => ['/company/index/'],
                                  'linkOptions'=>[
                                                'target'=>'_blank',
                                                  ],
                                ],                            
                            ],
                        ],
                        [
                            'label' => 'SERVICES',
                            'items' => [
                                ['label' => 'Resume writing', 'url' => ['/resume/index/']],
                                // ['label' => 'Advance Search', 'url' => ['/job/advance-search']],
                            ],
                        ],
                        [
                            'label' => 'Masters',
                            'items' => [
                                ['label' => 'City', 'url' => ['/city/index/']],
                                // ['label' => 'Company', 'url' => ['/company/index/']],
                                ['label' => 'Country ', 'url' => ['/country/index/']],
                                ['label' => 'Course', 'url' => ['/course/index/']],
                                ['label' => 'Desired Career Profile', 'url' => ['/desired-career-profile/index/']],
                                ['label' => 'Education', 'url' => ['/education/index/']],
                                ['label' => 'Functional Area ', 'url' => ['/functional-area/index/']],
                                ['label' => 'Industry', 'url' => ['/industry/index/']],
                                ['label' => ' Online Profile', 'url' => ['/online-profile/index/']],
                                ['label' => 'Personal Details', 'url' => ['/personal-detail/index/']],
                                ['label' => 'Pincode', 'url' => ['/pincode/index/']],
                                ['label' => 'Profile ', 'url' => ['/profile/index/']],
                                ['label' => 'Project ', 'url' => ['/project/index/']],
                                ['label' => 'Qualification ', 'url' => ['/qualification/index/']],
                                ['label' => 'skill ', 'url' => ['/skill/index/']],
                                ['label' => 'Specialization ', 'url' => ['/specialization/index/']],
                                ['label' => 'State ', 'url' => ['/state/index/']],

                            ],
                        ],
                        [
                          'label' =>'Login',
                          'url' => ['/site/login'],
                          // 'options'=> ['class'=>'login'],
                          'linkOptions'=>[
                            'class'=>'login-modal',
                            'onclick'=>'login();return false;',
                            'data-toggle' =>'modal',
                            'data-target' =>'#myModal'
                          ],
                        ],
                      ],
            ]);
            NavBar::end();

    }else{

        $userLevel = UserLevel::find()->where(['user_level_id' => Yii::$app->user->identity['level_id']])->one();
        $userName = Yii::$app->user->identity['name'];
            NavBar::begin([
                'brandLabel' => '<b>JOBGURU</b>',
                // 'brandUrl' => Yii::$app->homeUrl,
                'brandUrl' => ['/site/index'],
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top nav-color',
                ],
            ]);
            
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [

                    [
                        'label' => 'JOBS',
                        'items' => [
                            ['label' => 'Search Jobs', 'url' => ['/job/index/']],
                            ['label' => 'Advance Search', 'url' => ['/job/advance-search']],
                        ],
                    ],
                    [
                        'label' => 'COMPANIES',
                        'items' => [
                            ['label' => 'All Companies', 'url' => ['/company/index/']],
                            // ['label' => 'Advance Search', 'url' => ['/job/advance-search']],
                        ],
                    ],
                    [
                        'label' => 'SERVICES',
                        'items' => [
                            ['label' => 'Resume writing', 'url' => ['/resume/index/']],
                            // ['label' => 'Advance Search', 'url' => ['/job/advance-search']],
                        ],
                    ],
                    [
                        'label' => 'Masters',
                        'items' => [
                            ['label' => 'City', 'url' => ['/city/index/']],
                            // ['label' => 'Company', 'url' => ['/company/index/']],
                            ['label' => 'Country ', 'url' => ['/country/index/']],
                            ['label' => 'Course', 'url' => ['/course/index/']],
                            ['label' => 'Desired Career Profile', 'url' => ['/desired-career-profile/index/']],
                            ['label' => 'Education', 'url' => ['/education/index/']],
                            ['label' => 'Functional Area ', 'url' => ['/functional-area/index/']],
                            ['label' => 'Industry', 'url' => ['/industry/index/']],
                            ['label' => ' Online Profile', 'url' => ['/online-profile/index/']],
                            ['label' => 'Personal Details', 'url' => ['/personal-detail/index/']],
                            ['label' => 'Pincode', 'url' => ['/pincode/index/']],
                            ['label' => 'Profile ', 'url' => ['/profile/index/']],
                            ['label' => 'Project ', 'url' => ['/project/index/']],
                            ['label' => 'Qualification ', 'url' => ['/qualification/index/']],
                            ['label' => 'skill ', 'url' => ['/skill/index/']],
                            ['label' => 'Specialization ', 'url' => ['/specialization/index/']],
                            ['label' => 'State ', 'url' => ['/state/index/']],

                        ],
                    ],
                    
                    ['label' => 'User', 'url' => ['/user/index']],
                    ['label' => 'Logout ('.$userName.':'.$userLevel['name'].')', 
                                'url' => ['/site/logout']],
                ],
            ]);
            NavBar::end();
        }
    ?>
    <div class="">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


<div class="modal fade" id="myModal" tabindex="-1"
      role="dialog" aria-labelledby="myModalLabel">
</div>
<script>
   function login(){
      $.ajax({
          type:'POST',
          url:'site/login',
          success: function(data)
               {
                   $('#myModal').html(data);
                   $('#myModal').modal();
               }
      });
   }
</script>