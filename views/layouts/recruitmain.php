<?php

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\RecruitAsset;
use yii\helpers\Url;
use app\models\Recruit;
use kartik\icons\Icon;
Icon::map($this);

RecruitAsset::register($this);

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<style type="text/css">

  ul, ol {
    margin: 0;
    padding: 13px;
  }

  .navbar-inverse {
    background-color: white;
    border-color: black;
    /*height: 0px;*/
  }
  .navbar-nav {
    float: left;
    margin: 0;
  }
  .navbar-inverse .navbar-nav > li > a {
    color: black;
    background-color: white;
  }
  .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus {
    color: #000;
    background-color: #ffffff;
  }

    .navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:hover, .navbar-inverse .navbar-nav > .open > a:focus {
      color: #000;
      background-color: #ffffff;
  }

  element.style {
    color: red;
    margin-top: -19px;
  }
  .navbar-nav > li > .dropdown-menu {
    margin-top: 0;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    /*width: 350px;*/
  }
</style>

<body>

  <?php $this->beginBody() ?>

<div class="wrap">

  <?php if(Yii::$app->recruit->isGuest) { ?>
  
  <?php

    NavBar::begin([
      'brandLabel' => Html::img('@web/images/guru1.png',
                      ['style'=> 'width: 200px; height: 50px;']),
      'brandUrl' => ['recruit/login'],
      'options' => ['class' => 'navbar-inverse navbar-fixed-top'],
    ]);

    echo Nav::widget([
      'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
          [ 
            'label' => 'Home',
            'url' => ['/recruit/login'],
            'linkOptions'=>['target'=>'_blank'],
          ],

          [ 
            'label' => 'Product',
            'items' =>[
              [
                'label' => 'Product on offer(Buy Online)',
                'url' => ['/recruit/buy-resume-database-access-packages']
              ],
              [
                'label' => 'Resume Database Access(RESDEX)',
                'url' => ['/recruit/buy-resume-database-access-packages']
              ],
              [
                'label' => 'Job Posting',
                'url' => ['/recruit/buy-resume-database-access-packages']
              ],
              [
                'label' => 'Recruitment Management System (RMS)',
                'url' => ['/recruit/buy-resume-database-access-packages']
              ],
              [
                'label' => 'Referal Solutions',
                'url' => ['/recruit/buy-resume-database-access-packages']
              ],
              [
                'label' => 'Branding Solutions',
                'url' => ['/recruit/buy-resume-database-access-packages']
              ],
            ]
          ]
        ],
    ]);

    echo Nav::widget([
      'options' => ['class' => 'navbar-nav navbar-right'],
      'encodeLabels' => false,
      'items' => [

          [

            'label' =>'<div style=margin-top:-20px;color:#71A1D6;font-size:10px;>Recruiter Toll Free(10AM to 6PM)</div>'. Icon::show(' fa-phone').'9654452911',
            'url' => ['/site/login'],
            'items' =>[

              [
                'label' => 'For Sales Enquiry ',
                'items' => [
                  '<div style=color:black;>India:</div>',
                  '<div>
                  <p>Toll Free :1800-200-100(9:30 AM to 6:30 PM)</p>
                  +91-5768768768::sales@guru.com
                  </div>',

                  '<div style=color:black;>USA:</div>',
                  '<div>
                  <p>Toll Free :1800-200-100(9:30 AM to 6:30 PM)</p>
                  +91-5768768768::sales@guru.com
                  </div>',

                  '<div style=color:black;>Europe/UK:</div>',
                  '<div>
                  <p>Toll Free :1800-200-100(9:30 AM to 6:30 PM)</p>
                  +91-5768768768::sales@guru.com
                  </div>',

                  '<div style=color:black;>Middle East/Africa & South East Asia:</div>',
                  '<div>
                  <p>Toll Free :1800-200-100(9:30 AM to 6:30 PM)</p>
                  +91-5768768768::sales@guru.com
                  </div>',
                  '<div style=color:black;>Recruiter Helpline:</div>',
                  '<div>
                  <p>Toll Free :1800-200-100(9:30 AM to 6:30 PM)</p>
                  +91-5768768768::sales@guru.com
                  </div>'
                ],
              ],

            ],
          ],


          [
            'label' => Icon::show('shopping-cart') .'&nbsp &nbsp &nbsp'.'Cart',
            'items' =>[
              '<div>&nbsp
              </div>'

            ],
          ],

          [
            'label' =>  'JobSeeker'.'&nbsp '. Icon::show('edit'),
            'linkOptions' => ['style' => 'color: #71A1D6;'],
            'url' => ['/site/login']
          ]

      ],
    ]);

    NavBar::end();

  }else{
    
    $userName = Yii::$app->recruit->identity['email'];

    NavBar::begin([
      'brandLabel' => Html::img('@web/images/guru1.png',['style'=> 'width: 200px; height: 50px;']),
      'brandUrl' => ['recruit/index'],
      'options' => ['class' => 'navbar-inverse navbar-fixed-top'],
    ]);

    echo Nav::widget([
      'options' => ['class' => 'navbar-nav navbar-left'],
      'encodeLabels' => false,
      'items' => [
          ['label' => 'Home', 'url' => ['/recruit/index']],

          [
              'label' => 'Jobs & Responses',
              'items' => [
                  [
                    'label' => 'Info',  
                  ],                                
              ],
          ],
          [
              'label' => 'Resdex',
              'items' => [
                  [
                    'label' => 'Info',  
                  ],                                
              ],
          ],
          [
              'label' => 'RSM/CSM',
              'items' => [
                  [
                    'label' => 'Info',  
                  ],                                
              ],
          ],
          [
              'label' => 'Reports',
              'items' => [
                  [
                    'label' => 'Info',  
                  ],                                
              ],
          ],
          [
              'label' => 'Referral/ IJP',
              'items' => [
                  [
                    'label' => 'Info',  
                  ],                                
              ],
          ],

          [
            'label' => Icon::show('clock fa-1x'),
          ],

          [
            'label' => Icon::show('bell fa-1x'),
          ],

          // [
          //   'label' => 'MY NAUKRI',
          //   'items' =>[
          //       [
          //         'label' => 'Edit Profile',
          //         'url' => ['/recruit/index'],
          //       ],
          //       [
          //         'label' => 'Logout ('.$userName.')', 
          //         'url' => ['/recruit/logout']
          //       ],
          //   ],                            
          // ],
      ],
    ]);

      echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
          [
            'label' => 'MY NAUKRI',
            'items' =>[
                [
                  'label' => 'Edit Profile',
                  'url' => ['/recruit/index'],
                ],
                [
                  'label' => 'Logout ('.$userName.')', 
                  'url' => ['/recruit/logout']
                ],
            ],                            
          ],
        ],
      ]);

    NavBar::end();
  }
  ?>
  
  <div class="container">
      <?= Alert::widget() ?>
      <?= $content ?>
  </div><br><br><br><br><br><br><br>
</div>
  <!-- Footer Start -->
  <footer class="jobguru-footer-area">
           <div class="footer-top section_50">
              <div class="container">
                 <div class="row">
                    <div class="col-lg-3 col-md-6">
                       <div class="single-footer-widget">
                          <div class="footer-logo">
                             <a href="index.html">
                             <img src="assets/img/logo.png" alt="jobguru logo">
                             </a>
                          </div>
                          <p>Aliquip exa consequat dui aut repahend vouptate elit cilum fugiat pariatur lorem dolor cit amet consecter adipisic elit sea vena eiusmod nulla</p>
                          <ul class="footer-social">
                             <li><a href="#" class="fb"><i class="fa fa-facebook"></i></a></li>
                             <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                             <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                             <li><a href="#" class="gp"><i class="fa fa-google-plus"></i></a></li>
                             <li><a href="#" class="skype"><i class="fa fa-skype"></i></a></li>
                          </ul>
                       </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                       <div class="single-footer-widget">
                          <h3>recent post</h3>
                          <div class="latest-post-footer clearfix">
                             <div class="latest-post-footer-left">
                                <img src="assets/img/footer-post-2.jpg" alt="post">
                             </div>
                             <div class="latest-post-footer-right">
                                <h4><a href="#">Website design trends for 2018</a></h4>
                                <p>January 14 - 2018</p>
                             </div>
                          </div>
                          <div class="latest-post-footer clearfix">
                             <div class="latest-post-footer-left">
                                <img src="assets/img/footer-post-3.jpg" alt="post">
                             </div>
                             <div class="latest-post-footer-right">
                                <h4><a href="#">UI experts and modern designs</a></h4>
                                <p>January 12 - 2018</p>
                             </div>
                          </div>
                       </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                       <div class="single-footer-widget">
                          <h3>main links</h3>
                          <ul>
                             <li><a href="#"><i class="fa fa-angle-double-right "></i> About jobguru</a></li>
                             <li><a href="#"><i class="fa fa-angle-double-right "></i> Delivery Information</a></li>
                             <li><a href="#"><i class="fa fa-angle-double-right "></i> Terms &amp; Conditions</a></li>
                             <li><a href="#"><i class="fa fa-angle-double-right "></i> Customer support</a></li>
                             <li><a href="#"><i class="fa fa-angle-double-right "></i> Contact with an expert</a></li>
                             <li><a href="#"><i class="fa fa-angle-double-right "></i> community updates</a></li>
                             <li><a href="#"><i class="fa fa-angle-double-right "></i> upcoming updates</a></li>
                          </ul>
                       </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                       <div class="single-footer-widget footer-contact">
                          <h3>Contact Info</h3>
                          <p><i class="fa fa-map-marker"></i> 4257 Street, SunnyVale, USA </p>
                          <p><i class="fa fa-phone"></i> 012-3456-789</p>
                          <p><i class="fa fa-envelope-o"></i> info@jobguru.com</p>
                          <p><i class="fa fa-envelope-o"></i> info@jobguru.com</p>
                          <p><i class="fa fa-fax"></i> 112-3456-7898</p>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
           <div class="footer-copyright">
              <div class="container">
                 <div class="row">
                    <div class="col-lg-12">
                       <div class="copyright-left">
                          <p>Copyright © 2018 Themescare. All Rights Reserved</p>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
  </footer>

  <!-- ************************************* -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
