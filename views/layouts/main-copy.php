<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\UserLevel;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
        if(Yii::$app->user->isGuest) {
    ?>
    <?php

        NavBar::begin([
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top nav-color',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right btn-back'],
                'items' => [

                    [
                        'label' => 'JOBS',
                        'items' => [
                            ['label' => 'Search Jobs', 'url' => ['/job/index/']],
                            ['label' => 'Advance Search', 'url' => ['/job/advance-search']],
                        ],
                    ],
                    
                    ['label' => 'Home', 'url' => ['/site/index']],
                    ['label' => 'Country', 'url' => ['/country/index']],
                    
                    ['label' => 'Login', 'url' => ['/site/login']]

                ]
            ]);
            NavBar::end();

    }else{

        $userLevel = UserLevel::find()->where(['user_level_id' => Yii::$app->user->identity['level_id']])->one();
        $userName = Yii::$app->user->identity['name'];
            NavBar::begin([
                'brandLabel' => '<b>JOBGURU</b>',
                // 'brandUrl' => Yii::$app->homeUrl,
                'brandUrl' => ['/site/index'],
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top nav-color',
                ],
            ]);
            
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Employment', 'url' => ['/employement-detail/index']],
                    ['label' => 'User', 'url' => ['/user/index']],
                    ['label' => 'Logout ('.$userName.':'.$userLevel['name'].')', 
                                'url' => ['/site/logout']],
                ],
            ]);
            NavBar::end();
        }
    ?> 

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
