<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Migrations */

$this->title = 'Create Migrations';
$this->params['breadcrumbs'][] = ['label' => 'Migrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="migrations-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
