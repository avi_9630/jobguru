<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\EmployementDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employement Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employement-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Employement Detail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'employment_detail_id',
            'current_designation',
            'current_company',
            'present_company',
            'annual_salary',
            //'working_from',
            //'present',
            //'work_till',
            //'state_id',
            //'notice_period',
            //'skill_id',
            //'industry_id',
            //'functional_area_id',
            //'role_id',
            //'job_discription',
            //'status',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
