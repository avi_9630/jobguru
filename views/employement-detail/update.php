<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmployementDetail */

$this->title = 'Update Employement Detail: ' . $model->employment_detail_id;
$this->params['breadcrumbs'][] = ['label' => 'Employement Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->employment_detail_id, 'url' => ['view', 'id' => $model->employment_detail_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="employement-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
