<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EmployementDetail */

$this->title = $model->employment_detail_id;
$this->params['breadcrumbs'][] = ['label' => 'Employement Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="employement-detail-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->employment_detail_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->employment_detail_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'employment_detail_id',
            'current_designation',
            'current_company',
            // 'present_company',
            'annual_salary',
            'thousands',
            'working_from',
            'month',
            'present',
            // 'work_till',
            'country_id',
            'city_id',
            'notice_period',
            'skill_id',
            // 'industry_id',
            // 'functional_area_id',
            // 'role_id',
            // 'job_discription',
            // 'status',
            // 'created_at',
            // 'updated_at',
        ],
    ]) ?>

</div>
