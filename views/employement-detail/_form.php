<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\City;
use app\models\Country;
use kartik\select2\Select2;
use yii\helpers\Url;

?>

<div class="employement-detail-form">
<div class="site-login">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="text-align:left;"> 
            
            <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'current_designation')->textInput(['maxlength' => true,'autofocus' => true,'autocomplete'=>"off",'placeholder' => 'Your Job Title']) ?>
                
                <?= $form->field($model, 'current_company')->textInput(['placeholder'=>'Where you are currently working ','maxlength' => true]) ?>

        <div class="row">
            <div class="col-md-6">
                <?php 
                $annualsalary = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,30,40,50);

                echo $form->field($model, 'annual_salary')->dropDownList($annualsalary,
                    [
                        'prompt'=>'Select'
                    ]);
                ?>
            </div>

            <div class="col-md-6">
                <?php 
                $thousands = array(0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95);

                echo $form->field($model, 'thousands')->dropDownList($thousands,
                    [
                        'prompt'=>'Select'
                    ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
            <?php echo $form->field($model, 'working_from')->dropDownList($model->getYearsList(),
                [
                    'id' => 'team-size',
                    'prompt'=>'Select Year '
                ]
                ); ?>
            </div>

            <div class="col-md-6">
                <?php
                $monthList = array( 1=>'Jauary',
                                    2=>'February',
                                    3=>'march',
                                    4=>'April' ,
                                    5=>'May',
                                    6=>'Jun',
                                    7=>'July',
                                    8=>'August',
                                    9=>'September',
                                    10=>'October',
                                    11=>'November',
                                    12=>'Decempber'
                                );

                echo $form->field($model, 'month')->dropDownList($monthList,
                [
                    'id' => 'team-size',
                    'prompt'=>'Select Month '
                ]);

                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?php echo $form->field($model, 'present')->dropDownList($model->getMonthList(),
                [
                    'id' => 'team-size',
                    'prompt'=>'Present'
                ]
                ); ?>
            </div>
            <div class="col-md-6"></div>
        </div>

            
            <?php
            $countryList=ArrayHelper::map(Country::find()->asArray()->all(), 'country_id', 'name');
            echo $form->field($model, 'country_id')->dropDownList($countryList,
                [
                    'prompt' => 'Select Country',
                    'id' => 'country_id',
                    'onchange' => 'getAllCity()',
                ]
            );
            ?>

            <div class="city">
                <?php

                $dataList=ArrayHelper::map(City::find()->asArray()->all(), 'city_id', 'name');
                echo $form->field($model, 'city_id')->dropDownList($dataList,
                    [
                        'prompt'=>'Select City',
                        'id' => 'depDropCity',
                    ]
                );
                ?>

            </div>

                <?php
                $notice = array( 1=>'15 days or less',
                                    2=>'1 month',
                                    3=>'2 months',
                                    4=>'3 months' ,
                                    5=>'More than 3 months'
                                );

                echo $form->field($model, 'notice_period')->dropDownList($notice,
                [
                    'id' => 'noticePeriod',
                    'prompt'=>'Select duration of your notice period '
                ]);

                ?>

                <?= $form->field($model, 'skill_id')->textInput() ?>
                
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
</div>


<script>

function getAllCity(){
    var country_id = document.getElementById("country_id").value;
    
    $.ajax({
        url: '<?php echo Url::to(["site/lists"]); ?>',
        type: 'post',
        data: {
            type : 6,
            id : country_id
        },
        success: function (res) {
            var nandghar = JSON.parse(res);
                var areaOption = "<option value=''>Select city</option>";
             for (var i = 0; i < nandghar.length; i++) {
                 areaOption += '<option value="' + nandghar[i]['id'] + '">' + nandghar[i]['name'] + '</option>'
             }
            $("#depDropCity").html(areaOption);

            if(document.getElementById("depDropCity") !== null){
                $("#depDropCity").val($("#depDropCity").val()).change();
            }
        },
        error: function (res) {
        }
    }); 
}
</script>