<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\EmployementDetailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employement-detail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'employment_detail_id') ?>

    <?= $form->field($model, 'current_designation') ?>

    <?= $form->field($model, 'current_company') ?>

    <?= $form->field($model, 'present_company') ?>

    <?= $form->field($model, 'annual_salary') ?>

    <?php // echo $form->field($model, 'working_from') ?>

    <?php // echo $form->field($model, 'present') ?>

    <?php // echo $form->field($model, 'work_till') ?>

    <?php // echo $form->field($model, 'state_id') ?>

    <?php // echo $form->field($model, 'notice_period') ?>

    <?php // echo $form->field($model, 'skill_id') ?>

    <?php // echo $form->field($model, 'industry_id') ?>

    <?php // echo $form->field($model, 'functional_area_id') ?>

    <?php // echo $form->field($model, 'role_id') ?>

    <?php // echo $form->field($model, 'job_discription') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
