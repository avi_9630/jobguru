<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Skill;
use app\models\EmployementDetail;
use app\models\Education;
// use app\models\Project;
use app\models\OnlineProfile;

/* @var $this yii\web\View */
/* @var $model app\models\Profile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'attach_resume')->fileInput()?>

    <?= $form->field($model, 'resume_headline')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'key_id')->dropDownList(ArrayHelper::map(Skill::find()->all(),'skill_id','name'),['prompt' => 'Select Skills']) ?>

    <?= $form->field($model, 'employement_detail_id')->dropDownList(ArrayHelper::map(EmployementDetail::find()->all(),'employement_detail_id','name'),['prompt' => 'Select Employement Datails']) ?>

    <?= $form->field($model, 'education_id')->dropDownList(ArrayHelper::map(Education::find()->all(),'education_id','name'),['prompt' => 'Select Educational Datails']) ?>

    <!--  -->

    <?= $form->field($model, 'profile_summary')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'online_profile_id')->textInput() ?>

    <?= $form->field($model, 'desired_career_profile_id')->textInput() ?>

    <?= $form->field($model, 'personal_detail')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
