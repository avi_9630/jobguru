<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OnlineProfile */

$this->title = 'Create Online Profile';
$this->params['breadcrumbs'][] = ['label' => 'Online Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="online-profile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
