<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OnlineProfile */

$this->title = 'Update Online Profile: ' . $model->online_profile_id;
$this->params['breadcrumbs'][] = ['label' => 'Online Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->online_profile_id, 'url' => ['view', 'id' => $model->online_profile_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="online-profile-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
