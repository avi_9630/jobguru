<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\PersonalDetailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="personal-detail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'personal_detail_id') ?>

    <?= $form->field($model, 'dob') ?>

    <?= $form->field($model, 'permanent_address') ?>

    <?= $form->field($model, 'gender') ?>

    <?= $form->field($model, 'hometown') ?>

    <?php // echo $form->field($model, 'pincode') ?>

    <?php // echo $form->field($model, 'marital_status') ?>

    <?php // echo $form->field($model, 'differently_abled') ?>

    <?php // echo $form->field($model, 'work_permit') ?>

    <?php // echo $form->field($model, 'work_permit_other_country') ?>

    <?php // echo $form->field($model, 'language') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
