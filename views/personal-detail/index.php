<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PersonalDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Personal Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Personal Detail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'personal_detail_id',
            'dob',
            'permanent_address',
            'gender',
            'hometown',
            //'pincode',
            //'marital_status',
            //'differently_abled',
            //'work_permit',
            //'work_permit_other_country',
            //'language',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
