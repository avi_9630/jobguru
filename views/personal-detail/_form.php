<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PersonalDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="personal-detail-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dob')->textInput() ?>

    <?= $form->field($model, 'permanent_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gender')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hometown')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pincode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'marital_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'differently_abled')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'work_permit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'work_permit_other_country')->textInput() ?>

    <?= $form->field($model, 'language')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
