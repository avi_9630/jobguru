<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pincode */

$this->title = 'Update Pincode: ' . $model->pincode_id;
$this->params['breadcrumbs'][] = ['label' => 'Pincodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pincode_id, 'url' => ['view', 'id' => $model->pincode_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pincode-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
