<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pincode */

$this->title = 'Create Pincode';
$this->params['breadcrumbs'][] = ['label' => 'Pincodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pincode-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
