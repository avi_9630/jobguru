<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FunctionalArea */

$this->title = 'Create Functional Area';
$this->params['breadcrumbs'][] = ['label' => 'Functional Areas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="functional-area-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
