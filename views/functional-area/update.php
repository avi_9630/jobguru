<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FunctionalArea */

$this->title = 'Update Functional Area: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Functional Areas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->functional_area_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="functional-area-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
