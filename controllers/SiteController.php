<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Recruit;
use yii\web\UploadedFile;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login','logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'main';
        return $this->render('index');
    }

    
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if($model->login()){
                return $this->redirect(['user/user-homepage']);
            }
        }
        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    
    public function actionRegister()
    {
        $model = new User;
        if ($model->load(Yii::$app->request->post())) {

            // get the instance varibale of file......
            $imageName = 'Resume'.rand(10,100).'_'.$model->name;
            $model->file = UploadedFile::getInstance($model , 'file');

            if($model->file){
                $imageName = $imageName.'.'.$model->file->extension;
                $model->resume = $imageName;
                $model->file->saveAs('../web/resume/'.$imageName);
            }

            $model->authKey = md5($model->password);
            $model->password = password_hash($model->password, PASSWORD_DEFAULT);

            if($model->save()){
                return $this->redirect(['employement-detail/create']);
            }            
        }else{
            return $this->render('register', [
                'model' => $model,
            ]);
        }
    }
    

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }


    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


// ************************************************************************************************
    // Get dependendlist For District Block Village
    public function actionLists()
    {
        $post = Yii::$app->request->post();
        if($post['type'] == 1){
            $query = new \yii\db\Query;
                     $query->select(['state_id','name'])  
                           ->from('state')
                           ->where(['country_id' => $post['id']]);
            $command = $query->createCommand();
            $operations = $command->queryAll();
            return json_encode($operations); 
        }

        if($post['type'] == 6){
            $query = new \yii\db\Query;
                     $query->select(['city_id','name'])  
                           ->from('city')
                           ->where(['country_id' => $post['id']]);
            $command = $query->createCommand();
            $operations = $command->queryAll();
            return json_encode($operations); 
        }
         
        if($post['type'] == 2) {
            $query = new \yii\db\Query;
                     $query->select(['id','rpc_name'])  
                           ->from('rpc_centre')
                           ->where(['district_id' => $post['id']]);
            $command = $query->createCommand();
            $operations = $command->queryAll();
            return json_encode($operations); 
      }
        if($post['type'] == 3){
            $query = new \yii\db\Query;
                     $query->select(['id','sub_cat_name'])  
                           ->from('sub_category')
                           ->where(['category_id' => $post['id']]);
            $command = $query->createCommand();
            $operations = $command->queryAll();
       
            return json_encode($operations); 
        }

         if($post['type'] == 4){
            $query = new \yii\db\Query;
                     $query->select(['id','name'])  
                           ->from('sub_sub_category')
                           ->where(['sub_cat_id' => $post['id']]);
            $command = $query->createCommand();
            $operations = $command->queryAll();
       
            return json_encode($operations); 
        }
        if($post['type'] == 5){
            $query = new \yii\db\Query;
                     $query->select(['id','sub_cat_name'])  
                           ->from('sub_category')
                           ->where(['category_id' => $post['id']]);
            $command = $query->createCommand();
            $operations = $command->queryAll();
            return json_encode($operations); 
        }


    }
    

    // Get Dependent List when update 

    public function actionGetDependentLists()
    {
       if ($id = Yii::$app->request->post('id')) {
                $operationPosts = \app\models\Nandghar::find()
                    ->where(['village_id' => $id])
                    ->count();
                if ($operationPosts > 0) {
                $query = new \yii\db\Query;
                $query->select(['village.id as villageId',
                                'district.id as districtId',
                                'block.id as blockId',
                                'state.id as stateId'
                               ])  
                ->from('state')
                ->innerJoin("district","district.state_id = state.id")
                ->innerJoin("block","block.district_id = district.id")
                ->innerJoin("village","village.block_id = block.id")
                ->where(['village.id' => $id]);
                $command = $query->createCommand();
                $operations = $command->queryAll();
                return json_encode($operations);
                    
            }
        }
    }

}
