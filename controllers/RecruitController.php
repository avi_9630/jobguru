<?php

namespace app\controllers;

use Yii;
use app\models\Recruit;
use app\models\search\RecruitSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use app\models\LoginForm;
use app\models\RecruitLogin;

/**
 * RecruitController implements the CRUD actions for Recruit model.
 */
class RecruitController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'recruitmain';
        $searchModel = new RecruitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

   
    public function actionCreate()
    {
        $model = new Recruit();

        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->recruit_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    
    protected function findModel($id)
    {
        if (($model = Recruit::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    // *********************Self action started from here*********************

    public function actionLogin()
    {
        $this->layout = 'recruitmain';
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['recruit/login']);
        }
        $model = new RecruitLogin();
        if ($model->load(Yii::$app->request->post())) {
            if($model->login()){
                return $this->redirect(['recruit/index']);
            }
            else{
                $model->getErrors();
            }
        }
        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);

    }

    public function actionLogout()
    {
        Yii::$app->recruit->logout();
        return $this->redirect(['recruit/login']);
    }    

    public function actionRegister()
    {
        $this->layout = 'recruitmain';
        $model = new Recruit;

        if ($model->load(Yii::$app->request->post())) {

            // get the instance varibale of file......
            
            $imageName = 'Employer'.rand(10,100).'_'.$model->company_name;
            $model->file = UploadedFile::getInstance($model , 'file');

            if($model->file){
                $imageName = $imageName.'.'.$model->file->extension;
                $model->photo = $imageName;
                $model->file->saveAs('../web/upload/'.$imageName);
            }
            $model->authKey = md5($model->password);
            $model->password = password_hash($model->password, PASSWORD_DEFAULT);

            if($model->save()){
                return $this->redirect(['recruit/login']);
            }            
        }else{
            return $this->render('register', [
                'model' => $model,
            ]);
        }
    }

    public function actionBuyResumeDatabaseAccessPackages()
    {
        $this->layout = 'recruitmain';
        $searchModel = new RecruitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



                //Drop down code here
// *************************************************************//

    public function actionLists()
    {
        $post = Yii::$app->request->post();
        if($post['type'] == 1){
            if ($id = $post['country_id']) {
                $countStates = \app\models\State::find()
                    ->where(['country_id' => $id])
                    ->count();

                 if ($countStates > 0) {
                    $query = new \yii\db\Query;
                             $query->select(['state_id','name'])  
                                   ->from('state')
                                   ->where(['country_id' => $id])
                                   ->orderBy(['name' => SORT_ASC]);
                    $command = $query->createCommand();
                    $operations = $command->queryAll();
                    return json_encode($operations);    
                 }   
            }
        }
        if($post['type'] == 2){
            if ($id = $post['state_id']) {
                $countBlocks = \app\models\City::find()
                   ->where(['state_id' => $id])
                   ->count();

                if ($countBlocks > 0) {
                    $query = new \yii\db\Query;
                             $query->select(['city_id','name'])  
                                   ->from('city')
                                   ->where(['state_id' => $id])
                                   ->orderBy(['name' => SORT_ASC]);
                    $command = $query->createCommand();
                    $operations = $command->queryAll();
                    return json_encode($operations);    
                }  
            }
        }
        if($post['type'] == 3)
        {
            if ($id = $post['city_id']) {
                $countBlocks = \app\models\Pincode::find()
                   ->where(['city_id' => $id])
                   ->count();

            if ($countBlocks > 0) {
                $query = new \yii\db\Query;
                         $query->select(['pincode_id','picode_number'])  
                               ->from('pincode')
                               ->where(['city_id' => $id])
                               ->orderBy(['picode_number' => SORT_ASC]);
                $command = $query->createCommand();
                $operations = $command->queryAll();
                return json_encode($operations); 
                }
            }
        }

    }
        // End Here


        // Get Dependent List when update 

      public function actionGetDependentLists()
        {
           if ($id = Yii::$app->request->post('id')) {
                    $operationPosts = \app\models\Nandghar::find()
                        ->where(['village_id' => $id])
                        ->count();
                    if ($operationPosts > 0) {
                    $query = new \yii\db\Query;
                    $query->select(['village.id as villageId',
                                    'district.id as districtId',
                                    'block.id as blockId',
                                    'state.id as stateId'
                                   ])  
                    ->from('state')
                    ->innerJoin("district","district.state_id = state.id")
                    ->innerJoin("block","block.district_id = district.id")
                    ->innerJoin("village","village.block_id = block.id")
                    ->where(['village.id' => $id]);
                    $command = $query->createCommand();
                    $operations = $command->queryAll();
                    return json_encode($operations);
                        
                }
            }
        }

}