<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\search\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class UserController extends Controller
{   
    public $enableCsrfValidation = false;
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    public function actionCreate()
    {
        $model = new User();
        
        if ($model->load(Yii::$app->request->post())) {

            // Start coding  for file uploading..

            //get the instance of uploaded file.
            $imageName = $model->name;
            $model->file = UploadedFile::getInstance($model,'file');
            $model->file->saveAs('resume/'.$imageName.'.'.$model->file->extension);

            // save the path in the db column

            $model->resume = 'resume/'.$imageName.'.'.$model->file->extension;

            // end of file uploading...

            $model->authKey = md5($model->password);
            $model->password = password_hash($model->password, PASSWORD_DEFAULT);
            
            if($model->save()){
                return $this->redirect(['index']);
            }else{
                print_r($model->getErrors());
                exit();
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if($_FILES['Resume']['name']){
                $iconName = 'Resume_'.rand(10,100).'_'.$model->name;
                $model->resume_file = UploadedFile::getInstance($model,'resume_file');
                if($model->resume_file){
                    $icon_name = $iconName.'.'.$model->resume_file->extension;
                    $model->resume = $icon_name;
                    $model->resume_file->saveAs('../web/resume/'.$icon_name);
                }
            }

            if($model->save()){
                return $this->redirect(['view', 'id' => $model->user_id]);
            }
            
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionForgot()
    {
        $model = new User();

        $form = Yii::$app->user->identity;
        $loadedPost = Yii::$app->request->post();
        if($loadedPost && $form->validate()){
            $user->password = $user->newPassword;
            $user->save(false);

        Yii::$app->session->setFlash('success','You have successfully change your password');
            return $this->refresh();
        }
        return $this->render('forgot',[
                'user' => $form,
        ]);

        if ($model->load(Yii::$app->request->post())) {
            $model->authKey = md5($model->password);
            $model->password = password_hash($model->password, PASSWORD_DEFAULT);
            if($model->save()){
                return $this->redirect(['index']);
            }else{
                print_r($model->getErrors());
                exit();
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionUserProfile(){
        $model = new User;
        return $this->render('user-profile');
    }

    public function actionUserHomepage()
    {
        $model = new User;
        // return $this->render('user-homepage');

        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){
                return $this->redirect(['user-homepage']);
            }else{
                print_r($model->getErrors());
                exit();
            }
        }
        return $this->render('user-homepage', [
            'model' => $model,
        ]);
    }

    
}
