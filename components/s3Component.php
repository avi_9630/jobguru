<?php 
namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use Aws\S3\S3Client;

class S3Component extends Component
{
    public function s3UrlFetch()
    {
        $post = Yii::$app->request->post();
        
        $mimeType = $post['mimeType'];
        $imageName = $post['imageName'];
        $key = 'AKIAJ2FIMTSSMRNPEWXQ';
        $secretKey = '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';
        $region = 'ap-south-1';
        $version = 'latest';
        $bucket = 'stggoatry';
        $acl = 'public-read';
        $alias = md5(uniqid()).".jpg";

        $s3Client = new S3Client([
            'region'  => $region,
            'version' => $version,
            'credentials' => [
                'key'    => $key,
                'secret' => $secretKey,
            ],
        ]);

        $cmd = $s3Client->getCommand('putObject', [
            'Bucket' => $bucket,
            'Key'    => $alias,
            'ContentType' => $mimeType,
            'ACL' => $acl,
            'Metadata' =>  [
                'name' => $imageName,
                'alias' => $alias,
                'type' => $mimeType,
            ]
        ]);
        
        $request = $s3Client->createPresignedRequest($cmd, '+20 minutes');
        $presignedUrl = (string) $request->getUri();
        $url = $s3Client->getObjectUrl($bucket, $alias);
        $data = [];
        if(isset($url) && isset($presignedUrl)){
            $data = [
                "url" => $presignedUrl,
                "file_url" => $url,
                "file_alias" => $alias,
                "file_name" => $imageName,
                "mime_type"=> $mimeType
            ];
        }
        
        return $data;
    }
}
        
?>